/*
 * CLI.c
 *
 *  Created on: Dec 1, 2021
 *      Author: avinoam.danieli
 *	 Receive using interrupt. ISR is looking for '\r', and notifys CLI task
 *  Transmit using DMA. Wait for DMA to be ready
 */

/* includes ----------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "defs.h"
#include "task.h"
#include "usart.h"
#include "cli.h"
#include "DWT_Test.h"
#include "EEPROM.h"

//#include "CLIcmnds.h"

CLI_command_T CommandToExe;
char errorMessageBuff[EMBLEN];

//char nullbuf[100];
//char errorMessageBuff[EMBLEN];

/********************************************************************
*	DMA_Ready
*
********************************************************************/

int32_t DMA_Ready(UART_HandleTypeDef *huart, int32_t delay)
{
	int32_t i;
	HAL_UART_StateTypeDef state;
		
	for (i = 0;i < delay;i ++)
	{
		state = HAL_UART_GetState(huart);
		if ( (state == (int)HAL_OK) | (state == (int)HAL_UART_STATE_READY) ) 
		{
			return TRUE; 
		}
		vTaskDelay(1);
	}
	return FALSE;
}

/********************************************************************
*	CLI_ConvertStringToCommand
*
********************************************************************/

int32_t CLI_ConvertStringToCommand(char* sRecieve)
{
	char *cp;

	if ( strlen(sRecieve) < 3)
		return (OKr);

	cp = strchr(sRecieve, '=');
	if (cp == NULL)
	{
		cp = strchr(sRecieve, ' ');			
		if (cp != NULL)
		{
			if ( EOF == sscanf(cp, "%s", CommandToExe.sCommand) )
			{
				sscanf(sRecieve, "%s", CommandToExe.sCommand);
				CommandToExe.ArgType = GETPAR;
			}
			else
			{
				sscanf(sRecieve, "%s", CommandToExe.sCommand);
				strcpy(CommandToExe.sParameter, cp);
				CommandToExe.ArgType = SETPAR;
			}
		}
		else
		{
			sscanf(sRecieve, "%s", CommandToExe.sCommand);
			CommandToExe.ArgType = GETPAR;
		}
	}
	else		// =
	{
		*cp = ' ';
		sscanf(sRecieve, "%s", CommandToExe.sCommand);
		strcpy(CommandToExe.sParameter, cp + 1);
		CommandToExe.ArgType = SETPAR;
	}
	//printf("Command %s\tParams %s\tArgType %d\n", CommandToExe.sCommand, CommandToExe.sParameter, CommandToExe.ArgType);
//	HAL_Delay(500);
	return (OKr);
}
 
void CLIErrorHandler(CLIRetCode_t errorFromList);
 
/********************************************************************
*	CLI
*	Task
*
********************************************************************/

TaskHandle_t RxFlg;
#define LASTMSGLEN 100
char lastmsg[LASTMSGLEN];
uint8_t RxBuff[RXLEN];
uint8_t TxBuff[TXLEN];
int32_t rx_isr_flg;

void CLI(void *a)
{
//	static int32_t next = 0;
//	int32_t	i;
//	int32_t crflg = 0;
//	int32_t eom = 0;
	uint32_t len;
	
	vTaskDelay(100);
   RxFlg = xTaskGetCurrentTaskHandle();										// for ISR
	strcpy(lastmsg, "VER\r");
	HAL_UART_Receive_IT(&huart3, RxBuff, RXLEN);	
	rx_isr_flg = 1;
	for(;;)
	{
		//		vTaskDelay(1000); continue;

		xTaskNotifyWait( 0x00, ULONG_MAX, &len, portMAX_DELAY );			// Wait for message from ISR
		HAL_UART_AbortReceive_IT(&huart3);

		if (RxBuff[0] == '\r')
			strncpy((char *)RxBuff, lastmsg, LASTMSGLEN);
		if (OKr==CLI_ConvertStringToCommand((char *)RxBuff))
		{
			CLIErrorHandler(CLI_ExecutCommand(&CommandToExe,  gCliCommandsList)); // ad
			if (GPF) printf("\nExcecute\n");
		}
		strncpy(lastmsg, (char *)RxBuff, LASTMSGLEN);
		memset(RxBuff, 0, len);
		HAL_UART_Receive(&huart3, RxBuff, 1, 0);
		while ( HAL_UART_Receive_IT(&huart3, RxBuff, RXLEN) != HAL_OK )
		{
			HAL_UART_AbortReceive_IT(&huart3);
		}		
		rx_isr_flg = 1;
	}
}

/********************************************************************
*
*	CLI_ExecutCommand
*
********************************************************************/
//char lastmsg[5][16];
//int32_t buffcnt = 0;
#define MSG_MAX_LEN 256
int32_t lastsize;
int32_t msgcnt = 0;
CLIRetCode_t CLI_ExecutCommand(CLI_command_T* pCommand ,  CLI_COMMAND_LIST_T const * pCommandList)
{
  	uint8_t i =0  ;
  	CLIRetCode_t RetCode=OKr;

	if ( (strncasecmp(pCommand->sCommand, "sideen", MSG_MAX_LEN) == 0) || 
		  (SideEn == TRUE) )
	{
		while (pCommandList[i].pName != NULL)
		{
			if (strncasecmp(pCommand->sCommand, pCommandList[i].pName, MSG_MAX_LEN) == 0)
			{
				// Call the function pointer in the command record.
				(void)pCommandList[i].pHandler(pCommand);
				RetCode =  OKr;
				break;
			}
			i++;
		}
		if (pCommandList[i].pName == NULL)
			RetCode =  COMMAND_NOT_FOUND;
		msgcnt ++;
	}
	else
		RetCode =  COMMAND_NOT_FOUND;
	return RetCode;
}

/********************************************************************
* HAL_UART_RxCpltCallback
*
********************************************************************/
int u;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	u++;	
}

/********************************************************************
* HAL_UART_TxCpltCallback
*
********************************************************************/
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	u++;
}

/********************************************************************
* errorHandler
*
* Write to SWO
* Write to CLI UART3
* Write to CLI UART6
* Save on Flash
* Disable interrupts
* Flash Blue LED and external red LED
*
********************************************************************/

void errorHandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION)
{
/*	sprintf(txebuff,  "File = %s  ", FILE);
	sprintf(txebuff+strlen(txebuff),  "Error line = %d  ", LINE);
	sprintf(txebuff+strlen(txebuff), "%s  ",  PRETTY_FUNCTION);
	sprintf(txebuff+strlen(txebuff), "  Error Value = %ld    ",  num);
	sprintf(txebuff+strlen(txebuff), "UPTIME = %lu \n\r", xTaskGetTickCount() / 1000);
	printf("%s", txebuff);
	vTaskDelay(5);
	HAL_UART_Transmit(&huart1, (uint8_t *)txebuff, strlen(txebuff), 1000L);
	HAL_UART_Transmit(&huart6, (uint8_t *)txebuff, strlen(txebuff), 1000L);  
*/	// leds, flash, _disable
	while(1)
	{
		HAL_GPIO_WritePin(LED_U_RED_GPIO_Port, LED_U_RED_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
		vTaskDelay(1000);
	}
}

/********************************************************************
* errorMessage
*
* Write to CLI UART3
* Write to CLI UART6
* Save on Flash
*
********************************************************************/

void errorMessage(char *str)
{
	vTaskDelay(5);
/*	HAL_UART_Transmit(&huart1, (uint8_t *)str, strlen(str), 1000L);
	HAL_UART_Transmit(&huart6, (uint8_t *)str, strlen(str), 1000L);
*/
}

/***************************************************************
*	CLIErrorHandler
***************************************************************/

void CLIErrorHandler(CLIRetCode_t errorFromList)
{
	uint16_t len;

	if (errorFromList)
  	{
		DMA_Ready(&huart3, 50);
			switch(errorFromList)
			{
			case OKr:
				break;
				case COMMAND_NOT_FOUND:
					len = snprintf((char *)TxBuff, TXLEN, "COMMAND_NOT_FOUND\r\n");
					HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
				break;
				case NO_AUTHORIZATION:
				break;
				case OUT_OF_LIMITS:
			break;
			case NO_permission:
			break;
			default:
				len = snprintf((char *)TxBuff, TXLEN, "ABNORMAL_BEHAVIOR\r\n");
				HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			break;
			}
		
  }
}
