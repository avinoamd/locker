/*
 * PLCcmnds.c
 *
 *  Created on: Aug 16, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tgmath.h>
#include "FreeRTOS.h"
#include "CLI.h"
//#include "LEDs.h"
#include "queue.h"
#include "PLC.h"
#include "mbcrc.h"
#include "BoardInit.h"
//#include "Safety.h"
#include "CLI.h"
#include "usart.h"
#include "adc.h"
#include "adcf.h"
#include "tim.h"
//#include "shift.h"
//#include "lsm6ds3.h"
//#include "PushButton.h"
//#include "Battery.h"
//#include "cmdpower.h"
#include "CLIops.h"
#include "semphr.h"
#include "varcalc.h"
#include "CLIcmnds.h"
#include "EEPROM.h"

//struct Modbus
//{
//	uint8_t *inbuff;
//	uint8_t *outbuff;
//	int32_t cnt;
//};
    
/*******************************************************************************
*	write_single_reg
*	6
*******************************************************************************/

void write_single_reg(STModbus *modbus)
{
   uint16_t addr, retval = 0;
	int16_t data;
   USHORT val;
   int32_t i;
   HAL_StatusTypeDef ret;

   val = usMBCRC16( modbus->inbuff, 8 );
   if (val == 0)
   {
	  for (i = 0;i < 8;i ++)								// prepare reply
		  modbus->outbuff[i] = modbus->inbuff[i];
	  i = 0;
	  addr =  (modbus->inbuff[3]) + (modbus->inbuff[2] << 8) ;
	  data =  (modbus->inbuff[5]) + (modbus->inbuff[4] << 8) ;
	  switch (addr)
	  {
		case 3:		
			Spd.dist = data;
		   break;
		case 5:		
			Spd.target = data;
		   break;
		case 12:		
			setpwm(data);
		   break;
		case 257:
		   break;
		case 258:
		   break;
		case 259:												//
		   break;
		case 260:												// On Off
		   break;
		case 360:																// locker position
//			param = (modbus->inbuff[5]) + (modbus->inbuff[4] << 8);
//			retval = plcmnd("Dist", 3, 1, param);
			break;
		case 361:																// locker position
			break;
		case 362:																// locker target
			break;
		case 363:																// locker target
			break;
		case 370:																// locker pwm
			break;
		case 371:																// locker pwm
			break;
		default:
			retval = 0xffff;
			modbus->outbuff[i+i+3] = (retval & 0xffff) >> 8;		// hi
			modbus->outbuff[i+i+4] = retval & 0x00ff;					// low
			break;
	  }	// switch
	  
	  if (retval == 0xffff)
	  {
		  modbus->outbuff[4] = 0xff;
		  modbus->outbuff[5] = 0xff;
	  }
	  val = usMBCRC16( modbus->outbuff, 6 );
	  modbus->outbuff[6] = val;
	  modbus->outbuff[7] = val >> 8;
	  *(modbus->cnt) += 1;
	  if( (ret = HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, 8)) != HAL_OK )	// reply
	  {
		  errorHandler(ret, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	  }
		  
   } 
   *(modbus->cnt_time) = xTaskGetTickCount();
}

/*******************************************************************************
*	write_discrete_output
*	5
*******************************************************************************/
uint16_t retval = 0;
void write_discrete_output(STModbus *modbus)
{
   uint16_t addr, data;
   USHORT crcval;
   int32_t i;
   int32_t stat = FALSE;
   //HAL_StatusTypeDef ret;
	//				CLI_command_T command;

   crcval = usMBCRC16( modbus->inbuff, 8 );

   if (crcval == 0)
   {
	  for (i = 0;i < 8;i ++)					// prepare reply
		  modbus->outbuff[i] = modbus->inbuff[i];
	  addr =  (modbus->inbuff[3] & 0x00ff) + (modbus->inbuff[2] << 8) ;
	  data =  (modbus->inbuff[5] & 0x00ff) + (modbus->inbuff[4] << 8) ;

	  switch (addr)
	  {
		case 0:										// Homing Lock position
				find_max();
				stat = TRUE;
//			   if (data == 0xFF00)
//				   stat = BrakeOn();
//			   if (data == 0x0000)
//			   {
//				   stat = TRUE;
//				   BrakeOff();
//			   }
		   break;
		case 1:										// Homing UnLock position
				find_min();
				stat = TRUE;
//				command.ArgType = SETPAR;
//				strcpy(command.sParameter, "1");
//				find_max_CLI(&command);
			   break;
		case 2:										// LED Green
				if (data == 0xFF00)
				{
				   HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET); 
					stat = TRUE;
				}
			   break;
		case 3:										// safety rst
			   break;
		case 6:										// Dir
				if (data == 0xFF00)
				{
				   HAL_GPIO_WritePin(DIR_GPIO_Port, DIR_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   HAL_GPIO_WritePin(DIR_GPIO_Port, DIR_Pin, GPIO_PIN_RESET); 
					stat = TRUE;
				}
			   break;
		case 7:										// enable, implemented only on Rev2
				if (data == 0xFF00)
				{
				   //HAL_GPIO_WritePin(ENABLE_GPIO_Port, ENABLE_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   //HAL_GPIO_WritePin(ENABLE_GPIO_Port, ENABLE_Pin, GPIO_PIN_RESET); 
					stat = TRUE;
				}
			   break;
		case 8:										// nBrake
				if (data == 0xFF00)
				{
				   HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_RESET); 
					Spd.uwFrequency = 0;
					stat = TRUE;
				}
			   break;
		case 9:										// Go Forever
		case 11:										// Go Forever
				if (data == 0xFF00)
				{
				   gof(1); 
					HAL_TIM_StateTypeDef var;
					var =	HAL_TIM_PWM_GetState(&htim3);
					printf("HAL_TIM_PWM_GetState(&htim3) = %d\n", var);
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   gof(0); 
					HAL_TIM_StateTypeDef var;
					var =	HAL_TIM_PWM_GetState(&htim3);
					printf("HAL_TIM_PWM_GetState(&htim3) = %d\n", var);
					stat = TRUE;
				}
			   break;
		case 13:										// LED Red
				if (data == 0xFF00)
				{
				   HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET); 
					stat = TRUE;
				}
			   break;
		case 14:										// LED User Red
				if (data == 0xFF00)
				{
				   HAL_GPIO_WritePin(LED_U_RED_GPIO_Port, LED_U_RED_Pin, GPIO_PIN_SET); 
					stat = TRUE;
				}
				else if (data == 0x0000)
				{
				   HAL_GPIO_WritePin(LED_U_RED_GPIO_Port, LED_U_RED_Pin, GPIO_PIN_RESET); 
					stat = TRUE;
				}
			   break;
		case 18:										//
		   break;
		case 40:										//
		   break;
		default:
		   break;
	  }

	  if (stat == TRUE)
	  {
		  //		  if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)modbus->outbuff, 8)!= HAL_OK)										// reply
		  *(modbus->cnt) += 1;
//		  if (addr == 20)
				//printf("outbuf= %d  %d  %d  %d  %d  %d\r\n", modbus->outbuff[0], modbus->outbuff[1], modbus->outbuff[2], modbus->outbuff[3], modbus->outbuff[4], modbus->outbuff[5]);

		  if( (/*ret = */HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, 8)) != HAL_OK )					// reply
		  {
			  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
	  }
     else
     {
   	  modbus->outbuff[4] = 0xff;
   	  modbus->outbuff[5] = 0x00;
    	  crcval = usMBCRC16( modbus->outbuff, 6 );
    	  modbus->outbuff[6] = crcval;
    	  modbus->outbuff[7] = crcval >> 8;
		  //		 if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)modbus->outbuff, 8)!= HAL_OK)										// reply
    	  *(modbus->cnt) += 1;
//		  if (addr == 20)
//    			printf("outbuf = %d  %d  %d  %d  %d  %d\r\n", modbus->outbuff[0], modbus->outbuff[1], modbus->outbuff[2], modbus->outbuff[3], modbus->outbuff[4], modbus->outbuff[5]);
		  if( (/*ret = */HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, 8)) != HAL_OK )					// reply
		  {
			  errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		  }
     }
   }
   *(modbus->cnt_time) = xTaskGetTickCount();
}

/*******************************************************************************
*	read_discrete_output
*	1
*
*******************************************************************************/

int klog = 1;
void read_discrete_output(STModbus *modbus)
{
   uint16_t addr, numofcoils, val;
   USHORT crcval;
   int32_t i, res;
   uint8_t len;

   crcval = usMBCRC16( modbus->inbuff, 8 );
   if (crcval == 0)
   {
	   addr = modbus->inbuff[3] + (modbus->inbuff[2] << 8);
	   numofcoils = modbus->inbuff[5] + (modbus->inbuff[4] << 8);
		if ( (addr < 16) && (numofcoils <= 16) )
		{
			modbus->outbuff[0] = modbus->inbuff[0];												// device address
			modbus->outbuff[1] = modbus->inbuff[1];												// functional code
			len = 1 + ((numofcoils-1) >> 3);
			modbus->outbuff[2] = (uint8_t)len;
			val = 0x0000;
			val ^= val;
			if (Spd.uwFrequency != 0)
			{
				val |= (1 << 11);																			// Go Forever
				val |= (1 << 9);																			// Go Forever
			}
			res = HAL_GPIO_ReadPin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);						// Green LED
			val |= (res << 2);																			// 
			res = HAL_GPIO_ReadPin(DIR_GPIO_Port, DIR_Pin);
			val |= (res << 6);																			// Dir
			res = HAL_GPIO_ReadPin(LCKR_EN3V3_GPIO_Port, LCKR_EN3V3_Pin);
			val |= (res << 7);																			// Enable
			res = HAL_GPIO_ReadPin(nBrake_GPIO_Port, nBrake_Pin);
			val |= (res << 8);																			// nBrake
			res = HAL_GPIO_ReadPin(LED_RED_GPIO_Port, LED_RED_Pin);							// Red LED
			val |= (res << 13);																			// 
			res = HAL_GPIO_ReadPin(LED_U_RED_GPIO_Port, LED_U_RED_Pin);						// Red User LED
			val |= (res << 14);																			// 
			
			
			val = val >> addr;
			val = val & (0xffff >> (16 -numofcoils));
			modbus->outbuff[4] = val >> 8;															// hi byte
			modbus->outbuff[3] = val & 0xff;															// lo byte

			len += 3;
			crcval = usMBCRC16( modbus->outbuff, len );
			modbus->outbuff[len] = crcval;
			modbus->outbuff[len+1] = crcval >> 8;
			//HAL_UART_StateTypeDef state;
			//state = HAL_UART_GetState(&huart4);
			//	   if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)modbus->outbuff, len+2)!= HAL_OK)			// reply
			*(modbus->cnt) += 1;
			if(HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, len+2)!= HAL_OK)			// reply
			{
				Error_Handler();
			}
//	   	vTaskDelay(50);
//	  	 vTaskDelay(10);
//	   	state = HAL_UART_GetState(&huart4);
//	  	 vTaskDelay(2);
//	  	 state = HAL_UART_GetState(&huart4);
//	   	state = HAL_UART_GetState(&huart4);
//			sprintf(txbuff,  "%d %d %d %d %d \r\n", modbus->outbuff[3], modbus->outbuff[4], modbus->outbuff[5], modbus->outbuff[6], modbus->outbuff[7]);

//			HAL_UART_Transmit(&huart3, (uint8_t *)txbuff, strlen(txbuff),100);
//			vTaskDelay(1);
//			HAL_UART_Transmit_DMA(&huart3, "\r\n", 2);
   	}
	}
   *(modbus->cnt_time) = xTaskGetTickCount(); 
}

/*******************************************************************************
*	read_analog_intput
*	4
*
*******************************************************************************/

void read_analog_intput(STModbus *modbus)
{
   uint16_t addr, num, curraddr;
   USHORT crcval;
   int32_t i, len = 0;
	uint16_t val;
   //int32_t first = 0;
   HAL_StatusTypeDef ret;
   
   crcval = usMBCRC16( modbus->inbuff, 8 );
   if (crcval == 0)
   {
	   addr = modbus->inbuff[3] + (modbus->inbuff[2] << 8);
	   num = modbus->inbuff[5] + (modbus->inbuff[4] << 8);
	   num = (uint16_t)fmin(num, 32);
	   modbus->outbuff[0] = modbus->inbuff[0];												// device address
	   modbus->outbuff[1] = modbus->inbuff[1];												// Functional code

	   modbus->outbuff[2] = (uint8_t)(num << 1);
	   memset(modbus->outbuff + 3, 0, num);													// clear
	   curraddr = addr;
		for (i = 0;i < num; i++)
		{
			switch (curraddr)
			{
			case 3:																						// dist
			case 4:																						// dist
				val = Spd.dist;
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 5:																						// target
				val = Spd.target;
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 12:																						// PWM
				val = getpwm();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 66:																						// FW_MAJOR
				modbus->outbuff[i+i+3] = 0; modbus->outbuff[i+i+4] = FW_MAJOR;
				break;
			case 67:																						// FW_MINOR
				modbus->outbuff[i+i+3] = 0; modbus->outbuff[i+i+4] = FW_MINOR;
				break;
			case 68:																						// Battery Voltage [mV]
				val = 54000;
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 69:																						// 
				val = VHall1();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 70:																						// 
				val = VHall2();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 71:																						// 
				val = currav;
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 72:																						// CPU Temperature [10m�C]
				val = CPUTmpr();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 73:																						// P3V3
				val = IntVref();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 74:																						// status word
				val = getstatusword();
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
			case 75:																						// heartbeat
				val = modbus->heartbit ++;
				modbus->outbuff[i+i+3] = val >> 8; 
				modbus->outbuff[i+i+4] = val & 0xff;
				break;
				
	
			case 261:																					// barcode reader
//				BCR_getdata(modbus->outbuff + 3, num);
				i += num;
				break;
			case 281:
				break;
			case 282:
				break;
			case 299:
				break;
			case 305:
				modbus->outbuff[i+i+3] = retval >> 8;								// hi
				modbus->outbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 306:
				modbus->outbuff[i+i+3] = retval >> 8;								// hi
				modbus->outbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 307:
				modbus->outbuff[i+i+3] = retval >> 8;								// hi
				modbus->outbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 308:
				modbus->outbuff[i+i+3] = retval >> 8;								// hi
				modbus->outbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 312:
				modbus->outbuff[i+i+3] = retval >> 8;								// hi
				modbus->outbuff[i+i+4] = retval & 0xff;							// low
				break;
			case 313:
				break;
				modbus->outbuff[i+i+3] = 0x00;
				modbus->outbuff[i+i+4] = 0x00;
				break;
			} // case
			curraddr ++;
		} // for
		//UNUSED(retval);
		len = num + num + 3;
		crcval = usMBCRC16( modbus->outbuff, len );
		modbus->outbuff[len] = crcval;
		modbus->outbuff[len+1] = crcval >> 8;
//   vTaskDelay(2);
//   if(HAL_UART_Transmit_IT(&huart4, (uint8_t*)modbus->outbuff, len+2)!= HAL_OK)					// reply
		*(modbus->cnt) += 1;
		if( (ret = HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, len+2)) != HAL_OK)// reply
		{
			snprintf(errorMessageBuff, EMBLEN, "HAL_UART_Transmit_DMA(modbus->huart Failed \r\n");
			errorMessage(errorMessageBuff);
			errorHandler(ret, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		}
   }
   *(modbus->cnt_time) = xTaskGetTickCount();
//   retrycnt = 0;
//   int gr;
//   gr = HAL_OK;
////   while(HAL_UART_Transmit_DMA(&huart4, (uint8_t*)modbus->outbuff, len+2)!= HAL_OK)			// reply
//   while(gr!= HAL_OK)			// reply
//   {
//   	gr = HAL_UART_Transmit_DMA(&huart4, (uint8_t*)modbus->outbuff, len+2);
//      retrycnt ++;
//   		//errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
//   }
//   if (retrycnt > 1)
//      	printf("RetryCnt = %d\n", retrycnt);
}

/*******************************************************************************
*	read analog output
*	3
*
*******************************************************************************/

void read_analog_output(STModbus *modbus)
{
   //uint16_t addr, data;
   USHORT val;
   HAL_StatusTypeDef ret;

   val = usMBCRC16( modbus->inbuff, 8 );												// check CRC
   if (val == 0)
   {
//	   addr = buff[3] + (buff[2] << 8);
//	   data =  (buff[8]) ;

	   modbus->outbuff[0] = modbus->inbuff[0];												// device address
	   modbus->outbuff[1] = modbus->inbuff[1];												// Functional code
	   modbus->outbuff[2] = modbus->inbuff[2];												// Address of the first register Hi bytes
	   modbus->outbuff[3] = modbus->inbuff[3];												// Address of the first register Lo bytes
	   modbus->outbuff[4] = 0;															// Value Hi bytes
	   modbus->outbuff[5] = 1;															// Value Lo bytes
//	   addr = buff[3] + (buff[2] << 8);
		val = usMBCRC16( modbus->outbuff, 6 );
		modbus->outbuff[6] = val;														// Checksum CRC
		modbus->outbuff[7] = val >> 8;												// Checksum CRC

 	   if( (ret = HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, 8)) != HAL_OK )			// reply
 	   {
 	   	errorHandler(ret, __FILE__, __LINE__, __PRETTY_FUNCTION__);
 	   }
 	   vTaskDelay(12000);		// ad, for SOM test only
   }
   *(modbus->cnt_time) = xTaskGetTickCount();
}

/*******************************************************************************
*	record_multiple_analog_outputs
*	16
*
*******************************************************************************/

void record_multiple_analog_outputs(STModbus *modbus)
{
   uint16_t addr, data;
   USHORT val;
   HAL_StatusTypeDef ret;

   val = usMBCRC16( modbus->inbuff, 11 );																	// check CRC
   if (val == 0)
   {
	   addr = modbus->inbuff[3] + (modbus->inbuff[2] << 8);
	   data =  (modbus->inbuff[8]) ;

	   modbus->outbuff[0] = modbus->inbuff[0];												// device address
	   modbus->outbuff[1] = modbus->inbuff[1];												// Functional code
	   modbus->outbuff[2] = modbus->inbuff[2];												// Address of the first register Hi bytes
	   modbus->outbuff[3] = modbus->inbuff[3];												// Address of the first register Lo bytes
	   modbus->outbuff[4] = modbus->inbuff[4];												// Number of registers Hi bytes
	   modbus->outbuff[5] = modbus->inbuff[5];												// Number of registers Lo bytes
		switch (addr)
		{
		case 20:										// LPM
		   break;
		case 21:										// Shutdown
		   break;
		}
		val = usMBCRC16( modbus->outbuff, 6 );
		modbus->outbuff[6] = val;														// Checksum CRC
		modbus->outbuff[7] = val >> 8;												// Checksum CRC
 	   if( (ret = HAL_UART_Transmit_DMA(modbus->huart, (uint8_t*)modbus->outbuff, 8)) != HAL_OK )			// reply
 	   {
 	   	errorHandler(ret, __FILE__, __LINE__, __PRETTY_FUNCTION__);
 	   }
   }
   *(modbus->cnt_time) = xTaskGetTickCount();
}

/*******************************************************************************
*	checkaddress
*
*******************************************************************************/

int32_t checkaddress(uint8_t addr)
{
	if ( (addr == 7) && (*pSide == 0x11111111) )
		return TRUE;
	else if ( (addr == 8) && (*pSide == 0x00000000) )
		return TRUE;
	else
		return FALSE;
}

/*******************************************************************************
*	PLCcmnds
*
*******************************************************************************/

void PLCcmnds(STModbus *modbus)
{

	if (checkaddress(modbus->inbuff[0]) == TRUE)
	{
		switch (modbus->inbuff[1])
		{
		case 1:									// read discrete output
			read_discrete_output(modbus);
			break;
		case 4:									// read analog input
			read_analog_intput(modbus);
			break;
		case 6:									// write discrete output
			write_single_reg(modbus);
			break;
		case 5:									// write discrete output
			write_discrete_output(modbus);
			break;
		case 16:									// record multiple analog outputs output
			record_multiple_analog_outputs(modbus);
			break;
		case 3:									// record multiple analog outputs output
			read_analog_output(modbus);
			break;
		default:
			break;
		}
	}
}

int64_t coilslo, coilshi;
void load_data()
{
	coilslo = 0;
	coilshi = 0;
	coilshi |= ( 1/*HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin)*/ << 0 );    	// 64
	coilshi |= ( 1/*HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin)*/ << 1 );    	// 65
}
/*
void test_02()
{

	uint8_t hi, lo;
	uint8_t hio = 0, loo = 0;

	hi = 0xa5;
	lo = 0xc3;
	// shift down 3
	loo = hi;
	loo = loo << 5;
	hio = hi >> 3;
	loo = loo | (lo >> 3);
	printf("%d %d %d %d\n", hi, lo, hio, loo);

}
*/
/*
void test_01()
{
uint16_t len, val1;
int i;

for (i = 0;i < 100;i ++)
	modbus->outbuff[i] = 61;

PLCinbuff[0] = 7;
PLCinbuff[1] = 1;
PLCinbuff[2] = 0;
PLCinbuff[3] = 64;
PLCinbuff[4] = 0;
PLCinbuff[5] = 8;
len = 6;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;

read_discrete_output(PLCinbuff);

PLCinbuff[3] = 63;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 58;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 57;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

PLCinbuff[3] = 57;
val1 = usMBCRC16( PLCinbuff, len );
PLCinbuff[len] = val1;
PLCinbuff[len+1] = val1 >> 8;
read_discrete_output(PLCinbuff);

}
*/

