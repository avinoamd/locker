/*
 * CommandList.c
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include "task.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "CLIops.h"
#include "adcf.h"
#include "BoardInit.h"
#include "varcalc.h"
#include "EEPROM.h"
/*#include "LEDs.h"
#include "cmdpower.h"
#include "safety.h"
#include "BCR.h"
#include <lsm6ds3.h>
#include "utils.h"
#include "print.h"
#include "BMS.h"
#include "eeprom_utils.h"
#include "PLC.h"
#include "bit.h"
#include "periodic.h"
#include "Battery.h"
#include "DAC.h"

#include "error.h"
*/
const CLI_COMMAND_LIST_T gCliCommandsList[] /*@ "FLASH"*/ =
{
	// name			function		#arg	permission	description
	//--------------------------------------------------------------------------
    { "??",				help,				0,		CLIENT,		"1-RND, 2-RND Only, 3-All" },           		 /*1*/
    { "HELP",			help,				0,		CLIENT,		"list commands" },            		/*2*/
//    { "RESET", 		reset,			0,		CLIENT,		"Do not preferom this command !!!!"},
//    { "RESET",		 	reset,			0,		DNP,			"Reset!!"},
//    { "IWDG", 			IWDG_en_CLI,	0,		CLIENT,		"Set IWDG Enable bit in EEPROM"},
//    { "IWDGCNT",		IWDG_cnt_CLI,	0,		CLIENT,		"IWDG reset counter"},
//    { "BL", 			bootloader,		0,		CLIENT,		"Enter BoorLoader"},
//	 { "TaskList",		tasklist,		0,		CLIENT,		"Task List"},
//	 { "RTS",			rts,				0,		CLIENT,		"Run time Status"},
//	 { "RRTS",			rrts,				0,		CLIENT,		"Reset Run time Status"},
	 { "lMSG",			message,			0,		CLIENT,		"Hello Avinoam"},
//  { "ERRORS", 		GetLaserErrors,	1,		CLIENT,		"Send string of error"},
	 { "VER",			GetVersion,		0,		CLIENT,		"get Version"},
//	 { "SerialNumber",SerialNumber,	0,		CLIENT,		"HW Serial Number"},
//	 { "MCUData"	  ,mcudata,			0,		CLIENT,		"MCU Data"},
	 { "UPTIME",		Uptime,			0,		CLIENT,		"UPTIME"},
//	 { "params_update",params_update,0,		CLIENT,		"Update parameters RAM->EEPROM"},
	 { "side", 			side, 			0, 	CLIENT,		"set/get side of Locker"},
	 { "sideen", 		sideEn, 			0, 	CLIENT,		"enable CLI"},
//
//
//	 { "     ",		NULL,				0,		CLIENT,		"      "},
//	 { "div0", 		div0, 			0, 	CLIENT,		"divide by zero"},
//	 { "memof", 	memof, 			0, 	CLIENT,		"memory out of range"},
//
//
//	 { "     ",		NULL,				0,		CLIENT,		"      "},
//	//---------- 	ADC    ---------------------------------------------------------
	{ "lTMPR",		CPUTmpr_CLI,	0,		CLIENT,		"Get CPU Temperature"},
	{ "lVREF",		IntVref_CLI,	0,		CLIENT,		"Get CPU Internal voltage reference"},
	{ "VHALL",		VHall_CLI,		0,		CLIENT,		"Magnet Hall Sensors Voltages"},
	{ "mhall",		mhall,			0,		CLIENT,		"Motor Hall Sensors Status"},
//	{ "vsw",			vsw,				0,		CLIENT,		"Get Switch Voltage"},
	{ "findmax",	find_max_CLI,	0,		CLIENT,		"Place arm in lock position 2 - Left, 1 - Right"},
	{ "max",			find_max_CLI,	0,		RND,			"Place arm in lock position L, R"},
	{ "findmin",	find_min_CLI, 	0,		CLIENT,		"Place arm in unlock position 1 - Right, 2 - Left"},
	{ "min",			find_min_CLI,	0,		RND,			"Place arm in unlock position L, R"},
	{ "varcalc",	varcalc_CLI,	0,		CLIENT,		"Noise of ADC inputs"},
//	{ "p5v",			p5v,				0,		CLIENT,		"Get P5V Voltage"},
//	{ "p24v",		p24v,				0,		CLIENT,		"Get P24V Voltage"},
//	{ "p3v3",		p3v3,				0,		CLIENT,		"Get P3V3 Voltage"},
//	{ "vcharge",	vcharge,			0,		RND,			"Get Charger Voltage"},
//	{ "vchargeint",vchargeint,		0,		RND,			"Get Charger Voltage miliVolts"},
	{ "curr",		curr_CLI,		0,		CLIENT,		"Get Motor Current"},
	{ "currlmt",	curr_limit_CLI,0,		CLIENT,		"Get/Set Motor Current Limit"},
//	{ "vbrake",		vbrake,			0,		CLIENT,		"Get Brakes Voltage"},
//	{ "vlock",		vlock,			0,		CLIENT,		"Get Locker Voltage"},
//	{ "GetV",		GetV,				0,		CLIENT,		"Get Voltages"},
//	{ "GetC",		GetC,				0,		CLIENT,		"Get Currents"},
//	{ "BMS_VBATT",	bms_vbatt,		0,		RND,			"Measure difference between BMS and VBatt, and store in Bms_Vbatt"},
//	{ "BTT",			bms_vbatt,		1,		RNDONLY,		"Measure difference between BMS and VBatt, and store in Bms_Vbatt"},
//	{ "Wire_Voltage",wires_voltage,0,	RND,			"Calculate voltage on battery wires"},
//	{ "WRV",			wires_voltage,	1,		RNDONLY,		"Calculate voltage on battery wires"},
//	{ "DAC",			DAC_CLI,			0,		CLIENT,		"Set/Get DAC"},
//	{ "     ",		NULL,				0,		CLIENT,		"      "},
//	//---------- 	Control    ---------------------------------------------------------
	{ "lLEDcmd",	LEDcmd,			0,		CLIENT,		"Set LED "},
	{ "enable",		enable,			0,		CLIENT,		"Driver Enable"},
	{ "PWM",			pwm,				0,		CLIENT,		"PWM 0 - 100%"},
	{ "SETDIR",		setdir,			0,		CLIENT,		"Motor Spin Direction"},
	{ "GETDIR",		getdir,			0,		CLIENT,		"Get Calculated Motor Spin Direction"},
	{ "GETSPD",		getspd,			0,		CLIENT,		"Get Calculated Motor Speed"},
	{ "nBrake",		nBrake,			0,		CLIENT,		"0 - Brake Motor, 1 - free"},
	{ "go",			go,				0,		CLIENT,		"0 - Brake Motor, 1 - free"},
	{ "dist",		dist,				0,		CLIENT,		"0 - Brake Motor, 1 - free"},
	{ "target",		target,			0,		CLIENT,		"0 - Brake Motor, 1 - free"},
//	{ "     ",		NULL,				0,		CLIENT,		"      "},
//	//---------- 	Power      ---------------------------------------------------------
//	{ "Locker_prechrg_en",locker_prechrg_en,	0,		CLIENT,		"Locker PreCharge Enable"},
//	{ "LPE",					locker_prechrg_en,	RND,	CLIENT,		"Locker PreCharge Enable"},
//	{ "prechrg_en",		prechrg_en,				0,		CLIENT,		"PreCharge Enable"},
//	{ "vsw_en",				vsw_en,					0,		CLIENT,		"VSW Enable"},
//	{ "vlocksw_en",		vlocksw_en,				0,		CLIENT,		"Locker SW Enable"},
//	{ "DCDC24V_en",		DCDC24V_en,				0,		CLIENT,		"DCDC24V Enable"},
//	{ "P5V_en",				P5V_en,					0,		CLIENT,		"P5V Enable"},
//	{ "Puller_en",			Puller_en,				0,		CLIENT,		"Puller Enable, 0 - Shut, 1 - On"},
//	{ "brake_open",		brake_en,				0,		CLIENT,		"Brake Open"},
//	{ "Charge_dis",		charge_en,				0,		RND,			"Charging Disable, 1 - disable, 0 - enable"},
//	{ "Pwrmd",				cli_powermode,			0,		CLIENT,		"0 -shutdown, 1 - low power mode , 2- on"},
//	{ "SOMrst",				som_rst,					0,		CLIENT,		"0 -SOM go, 1 - SOM Reset"},
//	{ "Key",					getkey,					0,		CLIENT,		"0 - Normal, 1 - Override"},
//	{ "STO",					getsto,					0,		CLIENT,		"STO State"},
//	{ "RELAY",				getrelay,				0,		CLIENT,		"Relay Control State"},
//	{ "DRVIN",				DRVIN,					0,		CLIENT,		"Get DRVIN State"},
//	{ "DRVOUT",				DRVOUT,					0,		CLIENT,		"Set DRVOUT State"},
//	{ "LockerOut",			LockerOut,				0,		CLIENT,		"Set Locker OUT State"},
//	{ "voltagecurr",		voltagecurr,			0,		CLIENT,		"Set Locker OUT State"},
//	{ "BMS_TEST",			BMS_test_CLI,			0,		CLIENT,		"I2C3 0 - 3"},
//	{ "PLC_TEST",			PLC_test_CLI,			0,		CLIENT,		"USART4"},
//	{ "BCR_TEST",			BCR_test_CLI,			0,		CLIENT,		"USART5"},
//	{ "       ",			NULL,						0,		CLIENT,		"      "},
//	//---------- 	Safety   --------------------------------------------------------------
//	{ "Safety_ALERT",	SafetyAlertCLI,		0,		CLIENT,	"Safety Alert"},
//	//---------- Barcode Reader ----------------------------------------------------------
//	{ "BCR_getdata",	BCR_CLI_getdata,		0,		CLIENT,	"BCR getdata"},
//	{ "BCR",				BCR_CLI_getdata,		1,		RNDONLY,	"BCR getdata"},
//	{ "BCREN",			BCR_CLI_en,				0,		CLIENT,	"BCR Enable"},
//	{ "IMU",				IMU_CLI_getdata,		0,		CLIENT,	"IMU getdata"},
//	{ "BMS",				BMS_CLI,					0,		CLIENT,	"BMS data"},
//	{ "SOC",				SOC_CLI,					0,		RND,		"State of Charge"},
//	{ "       ",		NULL,						0,		CLIENT,	"      "},
//	//---------- 	Parameters   ----------------------------------------------------------
//	{ "BrakeCurrent",		Brake_Current,			0,		RND,	"Set Brake Current"},
//	{ "BC",					Brake_Current,			1,		RNDONLY,	"Set Brake Current"},
//	{ "       ",			NULL,						0,		CLIENT,		"      "},
//	//---------- 	BIT   -----------------------------------------------------------------
//	{ "Continuous_BIT_Reg",	Continuous_BIT_Register_CLI,		0,		RND,		"Continuous BIT Register"},
//	{ "CBR",						Continuous_BIT_Register_CLI,		0,		RNDONLY,	"Continuous BIT Register"},
//	{ "Powerup_BIT_Reg",		Powerup_BIT_Register_CLI,			0,		RND,		"Powerup BIT Register"},
//	{ "PBR",						Powerup_BIT_Register_CLI,			1,		RNDONLY,	"Powerup BIT Register"},
    {NULL},   // __line
};
