/*
 * varcalc.c
 *
 *  Created on: 10 Jan 2022
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "task.h"
#include "CLI.h"
#include "usart.h"
#include "adcf.h"
#include "boardinit.h"
#include "tim.h"
#include "EEPROM.h"

int32_t *hxav;

extern int32_t h2av;
extern int32_t h1av;
double mean1, var1;
double mean2, var2;
double mean3, var3;
double mean4, var4;
double mean5, var5;
double mean6, var6;
double mean7, var7;
void varcalc(void)
{
	double s1x = 0, s1x2 = 0;
	double s2x = 0, s2x2 = 0;
	double s3x = 0, s3x2 = 0;
	double s4x = 0, s4x2 = 0;
	double s5x = 0, s5x2 = 0;
	double s6x = 0, s6x2 = 0;
	double s7x = 0, s7x2 = 0;
	uint32_t i;
	double value;
	
	for (i = 0;i < 1024;i ++)
	{
		HAL_Delay(1);
		value = ADC_DMA_Buffer[HALL1];
		s1x += value;
		s1x2 += value * value;
		HAL_Delay(1);
		value = ADC_DMA_Buffer[HALL2];
		s2x += value;
		s2x2 += value * value;
		HAL_Delay(1);
		value = ADC_DMA_Buffer[CURR];
		s3x += value;
		s3x2 += value * value;
		HAL_Delay(1);
		value = ADC_DMA_Buffer[INTR_TMPR];
		s4x += value;
		s4x2 += value * value;
		HAL_Delay(1);
		value = ADC_DMA_Buffer[INTR_VREF];
		s5x += value;
		s5x2 += value * value;
		HAL_Delay(1);
		value = h2av;
		s6x += value;
		s6x2 += value * value;
		HAL_Delay(1);
		value = h1av;
		s7x += value;
		s7x2 += value * value;
	}
	mean1 = s1x / 1024.0;
	var1 = (s1x2 / 1024.0) - mean1 * mean1;
	printf("HALL1 = %g  %g\n", mean1, var1);
	mean2 = s2x / 1024.0;
	var2 = (s2x2 / 1024.0) - mean2 * mean2;
	printf("HALL2 = %g  %g\n", mean2, var2);
	mean3 = s3x / 1024.0;
	var3 = (s3x2 / 1024.0) - mean3 * mean3;
	printf("CURR  = %g  %g\n", mean3, var3);
	mean4 = s4x / 1024.0;
	var4 = (s4x2 / 1024.0) - mean4 * mean4;
	printf("INTR_TMPR  = %g  %g\n", mean4, var4);
	mean5 = s5x / 1024.0;
	var5 = (s5x2 / 1024.0) - mean5 * mean5;
	printf("INTR_VREF  = %g  %g\n", mean5, var5);
	mean6 = s6x / 1024.0;
	var6 = (s6x2 / 1024.0) - mean6 * mean6;
	printf("h2av  = %g  %g\n", mean6, var6);
	mean7 = s7x / 1024.0;
	var7 = (s7x2 / 1024.0) - mean7 * mean7;
	printf("h1av  = %g  %g\n", mean7, var7);
}

/*******************************************************************************
*	calcvar_CLI
*	CLI
*	
*******************************************************************************/

int32_t varcalc_CLI(CLI_command_T* command)
{
	uint32_t len;

	if ( command->ArgType == GETPAR)
  	{
		varcalc();
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s\r\nHALL1 = %g  %g\n\r ",command->sCommand, 
							 mean1, var1);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "HALL2 = %g  %g\n\r ", mean2, var2);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "CURR = %g  %g\n\r ", mean3, var3);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "INTR_TMPR = %g  %g\n\r ", mean4, var4);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "INTR_VREF = %g  %g\n\r ", mean5, var5);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "h2av = %g  %g\n\r ", mean6, var6);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "h1av = %g  %g\n\r ", mean7, var7);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	find_max_CLI
*	CLI
*	Description :
* 	rotate motor
*	Start find_max_isr
*
*******************************************************************************/

int32_t find_max_flg = FALSE;
int32_t find_min_flg = FALSE;
extern HAL_StatusTypeDef gostatus;

int32_t find_max_CLI(CLI_command_T* command)
{
	uint32_t len;
	int32_t num;
	static int32_t side = RightLocker;  			// R

	if ( command->ArgType == GETPAR)
	{
		if (RightLocker == getside())
		{
			hxav = &h2av;
			side = RightLocker;
		}
		else if (LeftLocker == getside())
		{
			hxav = &h1av;
			side = LeftLocker;
		}
		else
		{
			hxav = &h2av;
			side = RightLocker;
		}
	}
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%d", &side);
		if (num >= 1)
		{
			if ( side == RightLocker )				// R
			{
				hxav = &h2av;
			}
			else if ( side == LeftLocker  )		// L
			{
				hxav = &h1av;
			}
		}
	}
	Spd.dist = 0;
	Spd.target = 700;
	
	find_max_flg = TRUE;
	find_min_flg = FALSE;
	HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_SET);
	gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	if (gostatus != HAL_OK)
	{
//		errorHandler(-30, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s Failed=%d \r\n",command->sCommand, 
						 side);
	}
	else
	{	
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d \r\n",command->sCommand, 
						 side);
	}
	HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
	return TRUE;
}

/*******************************************************************************
*	find_min_CLI
*	CLI
*	Description :
* 	rotate motor
*	Start find_min_isr
*
*******************************************************************************/

int32_t find_min_CLI(CLI_command_T* command)
{
	uint32_t len;
	int32_t num;
	static int32_t side = RightLocker;  			// R

	if ( command->ArgType == GETPAR)
  	{
		if (RightLocker == getside())
		{
			hxav = &h2av;
			side = RightLocker;
		}
		else if (LeftLocker == getside())
		{
			hxav = &h1av;
			side = LeftLocker;
		}
		else
		{
			hxav = &h2av;
			side = RightLocker;
		}
	}
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%d", &side);
		if (num >= 1)
		{
			if ( side == RightLocker )				// R
			{
				hxav = &h2av;
			}
			else if ( side == LeftLocker  )		// L
			{
				hxav = &h1av;
			}
		}
	}
	Spd.dist = 0;
	Spd.target = 700;
	
	find_min_flg = TRUE;
	find_max_flg = FALSE;
	HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_SET);
	gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	if (gostatus != HAL_OK)
	{
//		errorHandler(-30, __FILE__, __LINE__, __PRETTY_FUNCTION__);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s Failed=%d \r\n",command->sCommand, 
						 side);
	}
	else
	{	
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d \r\n",command->sCommand, 
						 side);
	}
	HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
	return TRUE;
}

/*******************************************************************************
*	find_max_isr
*	
*	Description
*	look for 4 consecutive negative differences
*	stop motor
*
*******************************************************************************/

int32_t s = 0;
int32_t upcnt = 0, dncnt = 0;
int32_t prev = 2048 * 32;
void find_max_isr(void)
{

	if (find_max_flg != TRUE)
		return;
	switch (s)
	{
	case 0:
		if (*hxav > prev)
		{
			s = 1;
		}
		break;
	case 1:
		if (*hxav >= prev)
		{
			upcnt ++;
			dncnt = 0;
		}
		else
		{
			upcnt = 0;
			dncnt ++;
		}	
		if (upcnt >= 4)
		{
			upcnt = 0;
			dncnt = 0;
			s = 2;
		}
		break;
	case 2:
		if (*hxav < prev)
		{
			dncnt ++;
		}
		else
		{
			dncnt = 0;
		}		
		if (dncnt >= 4)
		{
			dncnt = 0;
			HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			s = 0;
			find_max_flg = FALSE;
		}
		break;
	default:
		s = 0;
		dncnt = 0;
		upcnt = 0;
		break;
	}
	prev = *hxav;
}

/*******************************************************************************
*	find_min_isr
*	
*	Description
*	look for 4 consecutive negative differences
*	stop motor
*
*******************************************************************************/

void find_min_isr(void)
{
	if (find_min_flg != TRUE)
		return;
	switch (s)
	{
	case 0:
		if (*hxav < prev)
		{
			s = 1;
		}
		break;
	case 1:
		if (*hxav <= prev)
		{
			dncnt ++;
			upcnt = 0;
		}
		else
		{
			dncnt = 0;
			upcnt ++;
		}	
		if (dncnt >= 4)
		{
			upcnt = 0;
			dncnt = 0;
			s = 2;
		}
		break;
	case 2:
		if (*hxav > prev)
		{
			upcnt ++;
		}
		else
		{
			upcnt = 0;
		}		
		if (upcnt >= 4)
		{
			upcnt = 0;
			HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			s = 0;
			find_min_flg = FALSE;
		}
		break;
	default:
		s = 0;
		dncnt = 0;
		upcnt = 0;
		break;
	}
	prev = *hxav;
}

// global, used to indicate first time of gof(1)
// used by gof(1), getstatusword() bit 3
int32_t S3M = 0;							
int32_t S0M = 0;							
int32_t S1M = 0;							

/*******************************************************************************
*	find_max
*	Description :
* 	rotate motor
*	Start find_max_isr
*
*******************************************************************************/

int32_t find_max(void)
{

	hxav = &h1av;
	Spd.dist = 0;
	Spd.target = 700;
	
	find_max_flg = TRUE;
	find_min_flg = FALSE;
	HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_SET);
	gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	if (gostatus != HAL_OK)
	{
//		errorHandler(-30, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	}
	S0M = 1;
	S1M = 0;
	return TRUE;
}

/*******************************************************************************
*	find_min
*	Description :
* 	rotate motor
*	Start find_max_isr
*
*******************************************************************************/

int32_t find_min(void)
{

	hxav = &h1av;
	Spd.dist = 0;
	Spd.target = 700;
	
	find_max_flg = FALSE;
	find_min_flg = TRUE;
	HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, GPIO_PIN_SET);
	gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	if (gostatus != HAL_OK)
	{
//		errorHandler(-30, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	}
	S1M = 1;
	S0M = 0;
	return TRUE;
}

/*******************************************************************************
*	getstatusword
*	
*	Description : get status word
*						bit0 - Homing done at lock position
*						bit1 - Homing done at unlock position
*						bit2 - Homing in proccess
*						bit3 - Position done 
*
*******************************************************************************/

#define POSITION_DONE_BIT 0x0008
#define Homing_Done_at_Lock_Position_BIT 0x0001 
#define Homing_Done_at_UnLock_Position_BIT 0x0002 
#define Homing_in_Process_BIT 0x0004 
uint16_t getstatusword(void)
{
	uint16_t state = 0;
	if (S3M != 0)
	{
		if ( (Spd.target == Spd.dist) || (Spd.target == 0x7fffffff) )
		{
			state |= POSITION_DONE_BIT;
		}
	}
	if (S0M != 0)
	{
		if (find_max_flg == FALSE)
		{
			state |= Homing_Done_at_Lock_Position_BIT;
		}		
	}
	if (S1M != 0)
	{
		if (find_min_flg == FALSE)
		{
			state |= Homing_Done_at_UnLock_Position_BIT;
		}		
	}
	if ( (find_max_flg == TRUE) || (find_min_flg == TRUE) )
	{
		state |= Homing_in_Process_BIT;
	}		
	
	
	return state;
}


/*******************************************************************************
*	calbtab
*	CLI
*	Description : calibration table
*	find lock
*	move motor step by step
*	add value to table
*
*******************************************************************************/

	
	
	
