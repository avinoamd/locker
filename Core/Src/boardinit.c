/*
 * boardinit.c
 *
 *  Created on: 10 Dec 2021
 *      Author: avinoam.danieli
 */
/*
	speed and placement calculation
*/
#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "task.h"
#include "CLI.h"
#include "adcf.h"
#include "usart.h"
#include "tim.h"
#include "boardinit.h"
#include "varcalc.h"

/*******************************************************************************
*	timer4_init
*	Description 		:	timer4_init
*	timer4 : motor speed 0.5 rps, 30 rpm 
*	interrupts both edge 180 interrups per second
*	timer clock 30MHz / 100, 300 KHz, count is 1667
*	HAL_TIM_IC_CaptureCallback occures 6 times per motor cycle (1 pole)
*
*******************************************************************************/

void timer4_init(void)
{
	// HAL_StatusTypeDef res;
	
	/*res =*/ HAL_TIM_IC_Start_IT(&htim4, TIM_CHANNEL_1);
	//res = HAL_TIM_IC_Start_IT(&htim4, TIM_CHANNEL_2);
	//res = HAL_TIM_IC_Start_IT(&htim4, TIM_CHANNEL_3);
}

/*******************************************************************************
*	speedcalc
*	Timer4 used for Hall Sensors
*	Timer3 used for PWM generation
*	Called by HAL_TIM_IC_CaptureCallback
*
*******************************************************************************/
Spd_t Spd = {0,0,0,0,0,0,0,0,0x7fffffff};
int bwdtab[] = {0,3,6,2,5,1,4,0};
int fwdtab[] = {0,5,3,1,6,4,2,0};
int MotorDir = 1;
void speedcalc(TIM_HandleTypeDef *htim)
{
	static int currstate = 5, prevstate= 1;
	
	if (htim == &htim4)													// Hall Sensors timer
	{
	  if ( (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) )
	  {
		 if(Spd.uhCaptureIndex == 0)
		 {
			/* Get the 1st Input Capture value */
			Spd.uwIC2Value1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
			Spd.uhCaptureIndex = 1;
		 }
		 else if(Spd.uhCaptureIndex == 1)
		 {
			 /* Get the 2nd Input Capture value */
			 Spd.uwIC2Value2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); 

			 /* Capture computation */
			 if (Spd.uwIC2Value2 > Spd.uwIC2Value1)
			 {
			    Spd.uwDiffCapture = (Spd.uwIC2Value2 - Spd.uwIC2Value1); 
			 }
			 else if (Spd.uwIC2Value2 < Spd.uwIC2Value1)
			 {
			    /* 0xFFFF is max TIM2_CCRx value */
			    Spd.uwDiffCapture = ((0xFFFF - Spd.uwIC2Value1) + Spd.uwIC2Value2) + 1;
			 }
			 else
			 {
			    /* If capture values are equal, we have reached the limit of frequency
				    measures */
			    //Error_Handler();
			 }
			 /* Frequency computation: for this example TIMx (TIM2) is clocked by
			    APB1Clk */    
			 if (Spd.uwDiffCapture != 0)
			 {
				 Spd.uwFrequency = HAL_RCC_GetPCLK1Freq() / Spd.uwDiffCapture;
				 Spd.avFrequency = ((Spd.avFrequency << 8) + Spd.uwFrequency - Spd.avFrequency) >> 8;
			 }
			 else
				 Spd.uwFrequency = 0;
			 Spd.uhCaptureIndex = 0;
		 }
		 currstate = HAL_GPIO_ReadPin(HA_GPIO_Port, HA_Pin) |  
							 (HAL_GPIO_ReadPin(HB_GPIO_Port, HB_Pin) << 1) |
							 (HAL_GPIO_ReadPin(HC_GPIO_Port, HC_Pin) << 2);
		 if (fwdtab[prevstate] == currstate)
		 {
			 MotorDir = 1;
			 Spd.dist ++;
		 }
		 else if (bwdtab[prevstate] == currstate)
		 {
			 MotorDir = 0;
			 Spd.dist --;
		 }
		 else
			 MotorDir = -1;
		 prevstate = currstate;
		 if (Spd.target == Spd.dist)
		 {
			  HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			  Spd.target = 0x7fffffff;
		 }
		 find_max_isr();
		 find_min_isr();
	  }
	}  
}
 
/*******************************************************************************
*	HAL_TIM_IC_CaptureCallback
*	Description 		:	
*	timer4 : motor speed 0.5 rps, 30 rpm 
*	interrupts both edge 180 interrups per second
*	timer clock 30MHz / 100, 300 KHz, count is 1667
*	HAL_TIM_IC_CaptureCallback occures 6 times per motor cycle (1 pole)
*
*******************************************************************************/

int32_t Log1, Log2;
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	static int s = 0;
	 
	speedcalc(htim);
/*	
	if (ADC_DMA_Buffer[HALL2] > h1)
		s = 1;
	h1 = 
*/	
	if (s++ < 1200000)
	{
		// printf("%d %d\n", ADC_DMA_Buffer[HALL2], ADC_DMA_Buffer[HALL1]);
//		Log2 = ADC_DMA_Buffer[HALL2];
//		Log1 = ADC_DMA_Buffer[HALL1];
		Log2 = h2av;
		Log1 = h1av;
	}
	if (s == 1200)
		printf("1200\n");
}

/*******************************************************************************
*	BoardInit
*	
*	Description 		:	Board Init
*
*******************************************************************************/

void BoardInit(void)
{
	ADC_DMA_Init();
	timer4_init();
	//varcalc();
}

/*******************************************************************************
*	LEDcmd
*	CLI
*	Description 		:	LEDcmd
*
*******************************************************************************/

int32_t LEDcmd(CLI_command_T* command)
{
	int32_t ledg, ledy, num, len;

	if (command->ArgType == GETPAR)
   {

	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d %d \n\r", command->sCommand, HAL_GPIO_ReadPin(LED_RED_GPIO_Port, LED_RED_Pin), HAL_GPIO_ReadPin(LED_U_RED_GPIO_Port, LED_U_RED_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld %ld", &ledg, &ledy);
		if (num >= 1)
			HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, ledg);
		if (num >= 2)
			HAL_GPIO_WritePin(LED_U_RED_GPIO_Port, LED_U_RED_Pin, ledy);
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d %d \n\r", command->sCommand, HAL_GPIO_ReadPin(LED_RED_GPIO_Port, LED_RED_Pin), HAL_GPIO_ReadPin(LED_U_RED_GPIO_Port, LED_U_RED_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   return FALSE;
}
