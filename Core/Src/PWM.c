/**
  ******************************************************************************
  * File Name          : PWM.c
  * Author             :
  * Date               :
  * Description        : 
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "task.h"

/*******************************************************************************
*	PWM
*	Task
*
*	Wait for semaphore
*	Start PWM
*	Monitor motor Hall count
*	Monitor arm Hall
*	Monitor current
*
*******************************************************************************/

void PWM(void)
{
	for(;;)
	{
		vTaskDelay(1000);
	}
}