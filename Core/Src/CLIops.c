/**
  ******************************************************************************
  * File Name          : CLIops.c
  * Author             :
  * Date               :
  * Description        : This file provides code for protocol
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "task.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "globals.h"
#include "ADC.h"
#include "usart.h"
#include "adcf.h"
#include "tim.h"
#include "boardinit.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* External variables --------------------------------------------------------*/

/* Private functions -------------------------------------------------------------------------------*/
/* Exported types ----------------------------------------------------------------------------------*/

/*******************************************************************************
*	enable
*******************************************************************************/

int32_t enable(CLI_command_T* command)
{
	int32_t len, gocmd, num;

	if (command->ArgType == GETPAR)
   {
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
								 HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, DRV_EN_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
	}
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &gocmd);
		if (num >= 1)
		{
			HAL_GPIO_WritePin(DRV_EN_GPIO_Port, DRV_EN_Pin, gocmd); 
			DMA_Ready(&huart3, 50);
	   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, DRV_EN_Pin)); 
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
   }
   return FALSE;
}

/*******************************************************************************
*	move
*******************************************************************************/

int32_t move(CLI_command_T* command)
{
	uint32_t len;
	
	DMA_Ready(&huart3, 50);
	{
		len = versionformain((char *)TxBuff);
		len += snprintf((char *)TxBuff+len, TXLEN, HWVER);
		len += datetimeformain((char *)TxBuff+len);
		len += snprintf((char *)TxBuff+len, TXLEN, " \r\n");
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
	}
}

/*******************************************************************************
*	getposition
*	CLI
*
*******************************************************************************/

int32_t getposition(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};
	uint32_t len;
	
	if ( command->ArgType == GETPAR)
	{
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%s \r\n",command->sCommand, MSG);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}

/*******************************************************************************
*	speed
*	CLI
*
*******************************************************************************/

int32_t speed(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};
	uint32_t len;
	
	if ( command->ArgType == GETPAR)
	{
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%s \r\n",command->sCommand, MSG);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}

/*******************************************************************************
*	status
*	CLI
*
*******************************************************************************/

int32_t status(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};
	uint32_t len;
	
	if ( command->ArgType == GETPAR)
	{
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%s \r\n",command->sCommand, MSG);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}

/*******************************************************************************
*	current
*	CLI
*
*******************************************************************************/

int32_t current(CLI_command_T* command)
{
	uint32_t len;
	uint16_t val;
	
	if ( command->ArgType == GETPAR)
	{
		val = ADC_DMA_Buffer[HALL1];
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%d \r\n",command->sCommand, val);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}

/*******************************************************************************
*	peakcurrent
*	CLI
*
*******************************************************************************/

int32_t peakcurrent(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};
	uint32_t len;
	
	if ( command->ArgType == GETPAR)
	{
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%s \r\n",command->sCommand, MSG);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}

/*******************************************************************************
*	mhall
*	CLI
*
*******************************************************************************/

int32_t mhall(CLI_command_T* command)
{
	int32_t len;

	if (command->ArgType == GETPAR)
   {

	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d %d %d\n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(HA_GPIO_Port, HA_Pin), 
							 HAL_GPIO_ReadPin(HB_GPIO_Port, HB_Pin),
							 HAL_GPIO_ReadPin(HC_GPIO_Port, HC_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	dist
*	CLI
*	Description : 	run motor
*	Motor control by PWM, nBrake
*  
*******************************************************************************/

int32_t dist(CLI_command_T* command)
{
	int32_t len, val, num;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 Spd.dist); 
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &val);
		if (num >= 1)
		{
			Spd.dist = val;
			DMA_Ready(&huart3, 50);
	   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 Spd.dist); 
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
   }
   return FALSE;
}

/*******************************************************************************
*	target
*	CLI
*	Description : 	run motor to specific distance
*  
*******************************************************************************/

int32_t target(CLI_command_T* command)
{
	int32_t len, val, num;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 Spd.target); 
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &val);
		if (num >= 1)
		{
			Spd.target = val; 
			DMA_Ready(&huart3, 50);
	   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 Spd.target); 
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
   }
   return FALSE;
}

/*******************************************************************************
*	gof
*	Description : 	run motor
*	Motor control by PWM, nBrake
*  
*******************************************************************************/

extern int32_t S3M;							
HAL_StatusTypeDef gostatus = -1;
int32_t gof(int32_t gocmd)
{
	
	if (gocmd == 1)
	{
		gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
		S3M = 1;
		//gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	}
	else if (gocmd == 0)
	{
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
		Spd.uwFrequency = 0;
	}
	else
		return FALSE;
	return TRUE;
}

/*******************************************************************************
*	go
*	CLI
*	Description : 	run motor
*	Motor control by PWM, nBrake
*  
*******************************************************************************/

int32_t go(CLI_command_T* command)
{
	int32_t len, gocmd, num;
	
	if (command->ArgType == GETPAR)
   {
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &gocmd);
		if (num >= 1)
		{
			if (gocmd == 1)
			{
				gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
				//gostatus = HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
			}
			else if (gocmd == 0)
			{
				HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			}
			DMA_Ready(&huart3, 50);
			len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
								 gocmd);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
   }
   return FALSE;
}

/*******************************************************************************
*	nBrake
*	CLI
*	Description : 	run motor
*	Motor control by PWM, nBrake
*  
*******************************************************************************/

int32_t nBrake(CLI_command_T* command)
{
	int32_t len, gocmd, num;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(nBrake_GPIO_Port, nBrake_Pin)); 
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &gocmd);
		if (num >= 1)
		{
			HAL_GPIO_WritePin(nBrake_GPIO_Port, nBrake_Pin, gocmd); 
			DMA_Ready(&huart3, 50);
	   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(nBrake_GPIO_Port, nBrake_Pin)); 
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
   }
   return FALSE;
}

/*******************************************************************************
*	getpwm
*	Description : 	get PWM 0 - 100%
*  conter set to 750. 30MHHz / 750 = 40KHz
*******************************************************************************/

int32_t getpwm(void)
{
	int32_t var;
	
	var = (sConfigOC3.Pulse << 1) / 15 + 1;
	if (sConfigOC3.Pulse == 0)
		var = 0;
	return var;
}

/*******************************************************************************
*	setpwm
*	Description : 	set PWM 0 - 100%
*  conter set to 750. 30MHHz / 750 = 40KHz
*******************************************************************************/

int32_t setpwm(int32_t var)
{
	
	if (var < 0)
		var = 0;
	else if (var > 100)
		var = 100;
	if (var >= 1)
	{
		sConfigOC3.Pulse = ((var * 15) >> 1) - 1;
	}
	else
	{
		sConfigOC3.Pulse = 0;
	}
	HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
	if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC3, TIM_CHANNEL_1) != HAL_OK)
	{
		Error_Handler();
	}
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	return TRUE;
}

/*******************************************************************************
*	pwm
*	CLI
*	Description : 	Set / get PWM 0 - 100%
*  conter set to 750. 30MHHz / 750 = 40KHz
*******************************************************************************/

int32_t pwm(CLI_command_T* command)
{
	int32_t len, var, num;
	
	if (command->ArgType == GETPAR)
   {
		var = getpwm(); 
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 var);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
	else
	{
	   num = sscanf(command->sParameter, "%ld", &var);
		if (num >= 1)
		{
			setpwm(var);
		}		
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 sConfigOC3.Pulse);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
 
}

/*******************************************************************************
*	setdir
*	CLI
*	Description : 	Set / get motor spin direction
*******************************************************************************/

int32_t setdir(CLI_command_T* command)
{
	int32_t len, dir, num;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(DIR_GPIO_Port, DIR_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   else if ( command->ArgType == SETPAR)
   {

	   num = sscanf(command->sParameter, "%ld", &dir);
		if (num >= 1)
			HAL_GPIO_WritePin(DIR_GPIO_Port, DIR_Pin, dir);
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(DIR_GPIO_Port, DIR_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	getdir
*	CLI
*	Description : 	get measured motor spin direction
*******************************************************************************/

int32_t getdir(CLI_command_T* command)
{
	int32_t len;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, 
							 HAL_GPIO_ReadPin(DIR_GPIO_Port, DIR_Pin));
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	getspd
*	CLI
*	Description : 	get measured motor speed
*******************************************************************************/

int32_t getspd(CLI_command_T* command)
{
	int32_t len;
	
	if (command->ArgType == GETPAR)
   {
	 	DMA_Ready(&huart3, 50);
   	len = snprintf ((char *)TxBuff, TXLEN, "%s %d %d \n\r", command->sCommand, 
							 Spd.uwFrequency / 600, Spd.avFrequency / 600);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
   	return TRUE;
   }
   return FALSE;
}









