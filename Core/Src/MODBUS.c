/*
 * SOM_MODBUS.c
 *
 *  Created on: 19 May 2020
 *      Author: avinoam.danieli
 */
/*
 * 	SOM Modbus communication via usart1 115200,N,8,1
 * 	ISR returns semaphor to detect first charactr in message
 *
 */
#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"

#include "port.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "globals.h"
#include "mbcrc.h"
#include "usart.h"
#include "PLC.h"
#include "MODBUS.h"
#include "tim.h"
//#include "print.h"

extern UART_HandleTypeDef huart3;
extern TIM_HandleTypeDef htim2;
uint8_t SOMinbuff[MODBUSINLEN];						// for SOM
uint8_t SOMoutbuff[MODBUSOUTLEN];					// for SOM

//uint8_t PLCoutbuff[PLCOUTLEN];						// for PLC
int32_t SOMrxflg;

/*******************************************************************************
*	uSec_Delay
*	Must call HAL_TIM_Base_Start(&htim10);
*	TIM_CR1_CEN               TIM_CR1_CEN_Msk                 !<Counter enable
*	before using uSec_Delay
*	Resolution 1 uSec
*	Range 0 - 65635 uSec
*******************************************************************************/

void uSecDelay(uint16_t us)
{
	uint16_t target;

	if (us == 0)
		return;
	target = us + __HAL_TIM_GET_COUNTER(&htim10);  		// set the counter value
	if (target < us)												// overflow
	{
	   __HAL_TIM_CLEAR_FLAG(&htim10, TIM_FLAG_UPDATE);
		while ( !__HAL_TIM_GET_FLAG(&htim10, TIM_FLAG_UPDATE) );		// wait to overflow
	}
	while ( __HAL_TIM_GET_COUNTER(&htim10) < target );					// wait
}

/*******************************************************************************
*	MODBUS
*	Task
*
*******************************************************************************/

uint32_t SOMcnt, SOMcntOut, SOMcntTime, SOMcntOutTime;
int32_t time_som1, time_som2, time_som3, time_som4;
int32_t len = 0xffff;
uint16_t llen, laddr;
#define MsgMODBUSLEN 100
char MsgMODBUS[MsgMODBUSLEN];

void MODBUS(void)
{

//
//   int len = 0xffff;
//   uint16_t llen, laddr;
   static int lasttime_som = 0;
   STModbus STSOMModbus = {SOMinbuff, SOMoutbuff, &SOMcntOut, &SOMcntOutTime, 0, &huart1};
   printf("sizeof(STSOMModbus) = %d\r\n", sizeof(STSOMModbus));

   xSemaphoreTake( BSemUART1_RX_ISRHandle, 10 );		// clear semaphor
   vTaskDelay(100);
   HAL_UART_AbortReceive_IT(&huart1);						// clear buffer
   HAL_UART_Receive(&huart1, (uint8_t *)SOMinbuff, MODBUSINLEN, MODBUSINLEN);		// assuming each byte take 1 mSec
//   xSemaphoreTake( BSemUART1_RX_ISRHandle, 10 );
   for(;;)
   {

   	len = 0xffff;
	   HAL_UART_AbortReceive_IT(&huart1);														// clear
	   SOMinbuff[0] = 0;																				// clear
	   int i; for (i = 0; i < 20; i ++) SOMinbuff[i] = 0;									// ad
	   SOMrxflg = 1;																					// first byte flag, reset by ISR
	   if(HAL_UART_Receive_IT(&huart1, (uint8_t *)SOMinbuff, 70) != HAL_OK)
	   {
		   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   }
	   xSemaphoreTake( BSemUART1_RX_ISRHandle, portMAX_DELAY );

	   time_som1 = DWT->CYCCNT;
	   while (len > huart1.RxXferCount)
	   {
		   len = huart1.RxXferCount;
		   uSecDelay(500);
	   }
	   HAL_Delay(6);
	   time_som2 = DWT->CYCCNT;
	   laddr = SOMinbuff[3] + (SOMinbuff[2] << 8);
	   llen = SOMinbuff[5] + (SOMinbuff[4] << 8);
	   SOMcnt++;
		SOMcntTime = xTaskGetTickCount();

		snprintf(MsgMODBUS, MsgMODBUSLEN, "%ld  %d\t%d   \t%ld\tXfercnt = %d  %ld  %ld\n", SOMcnt, laddr, llen, 70-len, huart1.RxXferCount, (time_som2-time_som1)/96, xTaskGetTickCount() - lasttime_som);
  		printf(MsgMODBUS);
   	lasttime_som = xTaskGetTickCount();
		//sprintf(txbuff,  "commbuf = %d %d %d %d %d \r\n", PLCinbuff[1], PLCinbuff[2], PLCinbuff[3], PLCinbuff[4], PLCinbuff[5]);

		//HAL_UART_Transmit(&huart3, (uint8_t *)txbuff, strlen(txbuff), 100);
	   time_som2 = DWT->CYCCNT;
	   PLCcmnds(&STSOMModbus);
	   time_som3 = DWT->CYCCNT - time_som2;
	   if (time_som3 > time_som4)
	   	time_som4 = time_som3;
	} 
}

/*
int flg = 0;
void PLC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	flg = 1;
}
*/
