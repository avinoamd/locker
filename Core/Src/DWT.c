/*
 * DWT.c
 *
 *  Created on: Aug 9, 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "defs.h"
int32_t GPF;

int32_t DWT_Test()
{
	uint32_t curr ;
	//RCC->
	curr = RCC->CSR;				// check if debugger connected
	//sprintf(msg, "%08x\r\n", curr);
	//HAL_UART_Transmit(&huart1, (uint8_t *)msg, strlen(msg), 100L);
	if ( (curr & 0x10000000U) == 0U )
	{
		GPF = FALSE;
		return (FALSE);
	}
	else
	{
		GPF = TRUE;
		return(TRUE);
	}
}
/**
 * @brief Initializes DWT_Cycle_Count for DWT_Delay_us function
 * @return Error DWT counter
 * 1: DWT counter Error
 * 0: DWT counter works

uint32_t DWT_Delay_Init(void);


 * @brief This function provides a delay (in microseconds)
 * @param microseconds: delay in microseconds

__STATIC_INLINE void DWT_Delay_us(volatile uint32_t microseconds)
{
 uint32_t clk_cycle_start = DWT->CYCCNT;
  Go to number of cycles for system
 microseconds *= (HAL_RCC_GetHCLKFreq() / 1000000);
  Delay till end
 while ((DWT->CYCCNT - clk_cycle_start) < microseconds);
}

#endif  DWT_STM32_DELAY_H_ */
