/*
 * EEPROM.c
 *
 *  Created on: 27 Jan 2022
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "usart.h"
#include "task.h"
#include "CLI.h"
#include "EEPROM.h"


/*******************************************************************************
*	getside
*	CLI
*	Description : 	get locker side from EEPROM
*  
*******************************************************************************/

int32_t getside(void)
{
	if (pSide[0] == 0x00000000)
	{
		return RightLocker;
	}
	else if (pSide[0] == 0x11111111)
	{
		return LeftLocker;
	}
	else
	{
		return -1;
	}	
}

/*******************************************************************************
*	side
*	CLI
*	Description : 	save locker side in EEPROM
*  
*******************************************************************************/

int32_t *pSide = (int32_t *)FLASH_EEPROM_BASE;
int32_t SideEn = TRUE;

int32_t side(CLI_command_T* command)
{
	int32_t len, val, num;
	HAL_StatusTypeDef estate;
	
	
	if (command->ArgType == GETPAR)
   {
		__NOP();
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &val);
		if (num >= 1)
		{
			estate = HAL_FLASHEx_DATAEEPROM_Unlock();
			if (estate == HAL_OK)
			{
				if (val == LeftLocker)							// Left
				{
					estate = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, (int32_t)pSide, 0x11111111); 
				}
				else if (val == RightLocker)					// Right
				{
					estate = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, (int32_t)pSide, 0x00000000); 
				}
				if (estate == HAL_OK)
				{
					estate = HAL_FLASHEx_DATAEEPROM_Lock();
					if (estate == HAL_OK)
					{
						DMA_Ready(&huart3, 50);
						len = snprintf ((char *)TxBuff, TXLEN, "%s %x \n\r", command->sCommand, pSide[0]); 
						HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
						return TRUE;
					}
				}
			}
			HAL_FLASHEx_DATAEEPROM_Lock();
		}
	}
	DMA_Ready(&huart3, 50);
	len = snprintf ((char *)TxBuff, TXLEN, "%s %x \n\r", command->sCommand, pSide[0]); 
	HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
	return TRUE;
}

/*******************************************************************************
*	sideEn
*	CLI
*	Description : 	save locker side in EEPROM
*  
*******************************************************************************/

int32_t sideEn(CLI_command_T* command)
{
	int32_t len, side, num;
	
	if (command->ArgType == GETPAR)
   {
		__NOP();
   }
   else if ( command->ArgType == SETPAR)
   {
	   num = sscanf(command->sParameter, "%ld", &side);
		if (num >= 1)
		{
			if (side == (pSide[0] & 0x00000001))
			{
				SideEn = TRUE;
			}
			else
			{
				SideEn = FALSE;
			}
		}				
	}
	DMA_Ready(&huart3, 50);
	len = snprintf ((char *)TxBuff, TXLEN, "%s %d \n\r", command->sCommand, SideEn); 
	HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
	return TRUE;
}


/*


  int *pi;
  int i, len;
  pi = (int *)0x08080000;
  for (i = 0;i < 12;i ++)
  {
	  arr[i] = pi[i];
	  len = sprintf((char *)TxBuff, "%x  ", pi[i]);
	  HAL_UART_Transmit(&huart3, TxBuff, len, len);
  }
  HAL_UART_Transmit_DMA(&huart3, "\n", 1);
  estat = HAL_FLASHEx_DATAEEPROM_Unlock();
  for (i = 0;i < 12;i ++)
	  estat = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, (int)(pi + i), 
														  (i + 1)+((i + 1) << 8) + ((i + 1) << 16) + ((i + 1) << 24));
  for (i = 0;i < 12;i ++)
  {
	  arr[i] = pi[i];
	  len = sprintf((char *)TxBuff, "%x  ", pi[i]);
	  HAL_UART_Transmit(&huart3, TxBuff, len, len);
  }
  HAL_UART_Transmit_DMA(&huart3, "\n", 1);
  for (i = 0;i < 12;i ++)
	  estat = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, (int)(pi + i), 
														  (i + i + 3) + ((i + i + 3) << 8) + ((i + i + 3) << 16) + ((i + i + 3) << 24));
  for (i = 0;i < 12;i ++)
  {
	  arr[i] = pi[i];
	  len = sprintf((char *)TxBuff, "%x  ", pi[i]);
	  HAL_UART_Transmit(&huart3, TxBuff, len, len);
  }
  HAL_UART_Transmit_DMA(&huart3, "\n", 1);
*/  
	  
  
  
  
  
