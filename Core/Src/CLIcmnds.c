/**
  ******************************************************************************
  * File Name          : CLIcmnds.c
  * Author             :
  * Date               :
  * Description        : This file provides code for protocol
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "task.h"
#include "CLI.h"
#include "CLIcmnds.h"
#include "globals.h"
#include "ADC.h"
#include "usart.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* External variables --------------------------------------------------------*/

/* Private functions -------------------------------------------------------------------------------*/
/* Exported types ----------------------------------------------------------------------------------*/

/*******************************************************************************
*	Uptime
*******************************************************************************/

int32_t Uptime(CLI_command_T* command)
{
	uint32_t len;

	DMA_Ready(&huart3, 50);     
	{
		len = snprintf((char *)TxBuff, TXLEN, "UPTIME=%lu    \n\r", HAL_GetTick() / 1000);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
	}
}

uint32_t versionformain(char *buff)
{
	return sprintf(buff,  SWVER);
}

uint32_t datetimeformain(char *buff)
{
	uint32_t len;
	
	// len = sprintf(buff, " %s",  __TIMESTAMP__);   
	len = sprintf((buff), " %s",  __DATE__);
	len += sprintf((buff+len), " %s",  __TIME__);
	return len;
}

/*******************************************************************************
*	GetVersion
*******************************************************************************/

int32_t GetVersion(CLI_command_T* command)
{
	uint32_t len;
	
	DMA_Ready(&huart3, 50);        
	{
		len = versionformain((char *)TxBuff);
		len += snprintf((char *)TxBuff+len, TXLEN, HWVER);
		len += datetimeformain((char *)TxBuff+len);
		len += snprintf((char *)TxBuff+len, TXLEN, " \r\n");
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
	}
}

/*********************************************************************
*	help
*********************************************************************/

int32_t help(CLI_command_T* command)
{
  	CLI_COMMAND_LIST_T *p = (CLI_COMMAND_LIST_T*)gCliCommandsList;
  	int32_t res, val;
	uint32_t len;

  	if (command->ArgType == GETPAR)
  	{
		while(p->pName)
		{
			if ( ( p->permission == CLIENT) || ( p->permission == RND) )
			{
				DMA_Ready(&huart3, 50);
				{
					len = snprintf((char *)TxBuff, TXLEN, "%-20s %s\n\r", p->pName, p->pDescription);
					HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
				}
			}
			p++;
		}
		return 0;
  	}
  	else if (command->ArgType == SETPAR)
  	{
		res = sscanf(command->sParameter, "%ld", &val);
		if (res > 0)
		{
			if (val == 1)							// RND
			{
				while(p->pName)
				{
					if ( p->permission == RND)
					{
						DMA_Ready(&huart3, 50);
						{
							len = snprintf((char *)TxBuff, TXLEN, "%-20s %s\n\r", p->pName, p->pDescription);
							HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
						}
					}
					p++;
				}
				return 0;
			}
			if (val == 2)							// RNDONLY
			{
				while(p->pName)
				{
					if ( p->permission == RNDONLY)
					{
						DMA_Ready(&huart3, 50);
						{
							len = snprintf((char *)TxBuff, TXLEN, "%-20s %s\n\r", p->pName, p->pDescription);
							HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
						}
					}
					p++;
				}
				return 0;
			}
			if (val == 3)							// All
			{
				while(p->pName)
				{
					if ( 1 )
					{
						DMA_Ready(&huart3, 50);
						{
							len = snprintf((char *)TxBuff, TXLEN, "%-20s %s\n\r", p->pName, p->pDescription);
							HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
						}
					}
					p++;
				}
				return 0;
			}
		}
  	}
  	return 0;									// Default
}

/*******************************************************************************
*	message
*	CLI
*
*******************************************************************************/

int32_t message(CLI_command_T* command)
{
	const static char MSG[] = {"Hello Avinoam\n"};
	uint32_t len;
	
	if ( command->ArgType == GETPAR)
	{
		DMA_Ready(&huart3, 50);
		{
			len = snprintf((char *)TxBuff, TXLEN, "%s=%s \r\n",command->sCommand, MSG);
			HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
			return TRUE;
		}
	}
	return FALSE;
}


