/*
 * adcf.c
 *
 *  Created on: 10 Dec 2021
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "defs.h"
#include "task.h"
#include "CLI.h"
#include "usart.h"
#include "cli.h"
#include "adc.h"
#include "tim.h"
#include "adcf.h"
#include "stm32l1xx_ll_adc.h"
#include "DWT_Test.h"
	 
/*******************************************************************************
*	ADC_DMA_Init
*
*******************************************************************************/

__IO uint16_t ADC_DMA_Buffer[ADC_DMA_Buffer_Len];

int32_t  ADC_DMA_Init(void)
{

	hadc.Instance->CR2 |= 0x30;						// 15 APB clock cycles after the end of conversion
	if ( HAL_ADC_Start_DMA(&hadc, (uint32_t *)ADC_DMA_Buffer, ADC_DMA_Buffer_Len) != HAL_OK )
   {
	   errorHandler(-3, __FILE__, __LINE__, __PRETTY_FUNCTION__);
	   return -17;
	}
	HAL_Delay(100);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	HAL_Delay(100);
	if (GPF) printf("%ld\n", /*status = */HAL_ADC_GetState(&hadc));
	return TRUE;
}

 /*******************************************************************************
*	curr_CLI
*	CLI
*
*******************************************************************************/

int32_t currav = 2048 << 5;
int32_t curr_CLI(CLI_command_T* command)
{
	int32_t h1, hav,  vref;
	uint32_t len;

	if ( command->ArgType == GETPAR)
  	{
		/*vref = __LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC_DMA_Buffer[INTR_VREF],
											 LL_ADC_RESOLUTION_12B);  */
		vref = 3300;  
		h1 = __LL_ADC_CALC_DATA_TO_VOLTAGE(vref,\
                                      ADC_DMA_Buffer[CURR],
                                      LL_ADC_RESOLUTION_12B);
		hav = __LL_ADC_CALC_DATA_TO_VOLTAGE(vref,\
                                      currav >> 5,
                                      LL_ADC_RESOLUTION_12B);
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d %d %d\r\n",command->sCommand,
							 	h1, hav, currav >> 5);
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
   }
   return FALSE;
}

 /*******************************************************************************
*	curr_limit_CLI
*	CLI
*
*******************************************************************************/

int32_t current_limit_var = 2180 << 5;
int32_t curr_limit_CLI(CLI_command_T* command)
{
	int32_t num, var;
	uint32_t len;

	if ( command->ArgType == SETPAR )
	{
	   num = sscanf(command->sParameter, "%ld", &var);
		if (num >= 1)
		{
			current_limit_var = var << 5;
		}
	}
	DMA_Ready(&huart3, 50);
	len = snprintf ((char *)TxBuff, TXLEN, "%s=%d \r\n",command->sCommand,
							current_limit_var >> 5);
	HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
	return TRUE;
}

/*******************************************************************************
*	getcputmpr
*	write again using STM32L-Discovery_FW_Pack
*  Calibrate VREF using internal Vref
*******************************************************************************/

int32_t gecputmpr(float *Tempr)
{
	int32_t val;
//	float TS_CAL1, TS_CAL2;

   val = ADC_DMA_Buffer[INTR_TMPR];
	*Tempr = (float)val;
	// VSENSE = (Vref/4095.0) * val;
	// inttmpr = ((VSENSE - V25) / Avg_Slope) + 25.0;
/*	TS_CAL1 = (float)(*TEMPSENSOR_CAL1_ADDR * VREFINT_CAL_VREF) / ( (float)VrefRatio * 1000.0 );
#ifdef PRINT
	printf("%d\n", *TEMPSENSOR_CAL1_ADDR);
	printf("%3.1f\n", (float)(*TEMPSENSOR_CAL1_ADDR) );
	printf("%d\n", *TEMPSENSOR_CAL2_ADDR);
	printf("%3.1f\n", (float)(*TEMPSENSOR_CAL2_ADDR) );

	printf("%d\t %3.3f\n", *VREFINT_CAL_ADDR, (float)(*VREFINT_CAL_ADDR) * Vref * 1000.0 / (float)(VREFINT_CAL_VREF) );
#endif
	TS_CAL2 = (float)(*TEMPSENSOR_CAL2_ADDR * VREFINT_CAL_VREF) / ( (float)VrefRatio * 1000.0 );
	*Tempr = ( (float)( (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP)*(val - TS_CAL1) ) / (float)(TS_CAL2 - TS_CAL1) ) + TEMPSENSOR_CAL1_TEMP;

*/	return TRUE;
}

 /*******************************************************************************
*	VHall1
*	
*******************************************************************************/

uint16_t VHall1(void)
{
	int32_t vref;

/*		vref = __LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC_DMA_Buffer[INTR_VREF],
											 LL_ADC_RESOLUTION_12B);	*/
	vref = 3300;
	return  __LL_ADC_CALC_DATA_TO_VOLTAGE(vref,\
											  ADC_DMA_Buffer[HALL1],
											  LL_ADC_RESOLUTION_12B);
}

 /*******************************************************************************
*	VHall2
*	
*******************************************************************************/

uint16_t VHall2(void)
{
	int32_t vref;

/*		vref = __LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC_DMA_Buffer[INTR_VREF],
											 LL_ADC_RESOLUTION_12B);	*/
	vref = 3300;
	return __LL_ADC_CALC_DATA_TO_VOLTAGE(vref,\
											  ADC_DMA_Buffer[HALL2],
											  LL_ADC_RESOLUTION_12B);
}

 /*******************************************************************************
*	VHall_CLI
*	CLI
*
*******************************************************************************/

int32_t VHall_CLI(CLI_command_T* command)
{
	uint32_t len;

	if ( command->ArgType == GETPAR)
  	{
/*		vref = __LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC_DMA_Buffer[INTR_VREF],
											 LL_ADC_RESOLUTION_12B);	*/
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d %d %d %d  \r\n",command->sCommand,
							 	ADC_DMA_Buffer[HALL1], ADC_DMA_Buffer[HALL2], 
								VHall1(), VHall2());
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	CPUTmpr
*	
*******************************************************************************/

int32_t CPUTmpr(void)
{
	int32_t vref;

		/*vref = __LL_ADC_CALC_VREFANALOG_VOLTAGE(ADC_DMA_Buffer[INTR_VREF],\
											 LL_ADC_RESOLUTION_12B);  */
	vref = 3300;
	return __LL_ADC_CALC_TEMPERATURE(vref,\
										 ADC_DMA_Buffer[INTR_TMPR],\
										 LL_ADC_RESOLUTION_12B);
	/*
__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(__TEMPSENSOR_TYP_AVGSLOPE__,\
                                             __TEMPSENSOR_TYP_CALX_V__,\
                                             __TEMPSENSOR_CALX_TEMP__,\
                                             __VREFANALOG_VOLTAGE__,\
                                             __TEMPSENSOR_ADC_DATA__,\
                                             __ADC_RESOLUTION__)
__LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(1.61,\
                                             626.8,\
                                             110,\
                                             3300,\
                                             ADC_DMA_Buffer[INTR_TMPR],\
                                             LL_ADC_RESOLUTION_12B)
*/
}



/*******************************************************************************
*	CPUTmpr_CLI
*	CLI
*
*******************************************************************************/

int32_t CPUTmpr_CLI(CLI_command_T* command)
{
	uint32_t len;

	if ( command->ArgType == GETPAR)
  	{
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d %d \r\n",command->sCommand, 
							 ADC_DMA_Buffer[INTR_TMPR], CPUTmpr());
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	IntVref
*	CLI
*
*******************************************************************************/

//int32_t Vref;
int32_t IntVref(void)
{
	int32_t val;

	val = ADC_DMA_Buffer[INTR_VREF];
	
	return __LL_ADC_CALC_VREFANALOG_VOLTAGE(val,  LL_ADC_RESOLUTION_12B);
		
}

/*******************************************************************************
*	IntVref_CLI
*	CLI
*
*******************************************************************************/

int32_t IntVref_CLI(CLI_command_T* command)
{
	uint32_t len;

	if ( command->ArgType == GETPAR)
  	{
		DMA_Ready(&huart3, 50);
		len = snprintf ((char *)TxBuff, TXLEN, "%s=%d  \r\n",command->sCommand, IntVref());
		HAL_UART_Transmit_DMA(&huart3, TxBuff, len);
		return TRUE;
   }
   return FALSE;
}

/*******************************************************************************
*	HAL_ADC_ConvCpltCallback
*	Callback
*	adc is triggered by timer3, 40KHz PWM.
*	This interrupt is every 50uSec
*******************************************************************************/

void current_limit(int32_t currav);
int32_t ADC_ConvCplt_Flg; // can be used by one task at a time
int32_t h2avx;
int32_t h1avx;
int32_t h2av = 2048 << 5;
int32_t h1av = 2048 << 5;
int32_t tm1, tm2, tm3, tm4;

uint32_t a1start, done;
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
		a1start = DWT->CYCCNT;

	if (ADC_ConvCplt_Flg == 0)
		tm1 = xTaskGetTickCount();
	if (ADC_ConvCplt_Flg == 999999)
		tm2 = xTaskGetTickCount();
	if ( (ADC_ConvCplt_Flg % 20) == 0 )		// this is for data log
	{
		h1avx = h1av;
		h2avx = h2av;
	}
	ADC_ConvCplt_Flg ++; 
	h2av = ((((uint32_t)h2av << 5) - h2av) + (ADC_DMA_Buffer[HALL2] << 5)) >> 5;	// Right Locker U2
	h1av = ((((uint32_t)h1av << 5) - h1av) + (ADC_DMA_Buffer[HALL1] << 5)) >> 5;	// Left Locker U1
	currav = ((((uint32_t)currav << 5) - currav) + (ADC_DMA_Buffer[CURR] << 5)) >> 5;	// Current
	current_limit(currav);
	done = DWT->CYCCNT - a1start;

}

/*******************************************************************************
*	current_limit
*	
*	if current is larger than limit then reduce pwm
*	if current is smaller than limit then increase pwm to pwm limit
*
*******************************************************************************/
void current_limit(int32_t currav)
{
	
	if (currav > current_limit_var)
	{
		if (htim3.Instance->CCR1 >= 30)
		{
			htim3.Instance->CCR1 --;
		}
	}
	else if ( (currav < current_limit_var) && (htim3.Instance->CCR1 < sConfigOC3.Pulse) )
	{
		htim3.Instance->CCR1 ++;
	}
}