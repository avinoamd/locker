/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
typedef StaticSemaphore_t osStaticSemaphoreDef_t;
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for Tcommrx */
osThreadId_t TcommrxHandle;
uint32_t commrxBuffer[ 256 ];
osStaticThreadDef_t commrxControlBlock;
const osThreadAttr_t Tcommrx_attributes = {
  .name = "Tcommrx",
  .cb_mem = &commrxControlBlock,
  .cb_size = sizeof(commrxControlBlock),
  .stack_mem = &commrxBuffer[0],
  .stack_size = sizeof(commrxBuffer),
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for TPWM */
osThreadId_t TPWMHandle;
uint32_t TPWMBuffer[ 512 ];
osStaticThreadDef_t TPWMControlBlock;
const osThreadAttr_t TPWM_attributes = {
  .name = "TPWM",
  .cb_mem = &TPWMControlBlock,
  .cb_size = sizeof(TPWMControlBlock),
  .stack_mem = &TPWMBuffer[0],
  .stack_size = sizeof(TPWMBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for MODBUSTask */
osThreadId_t MODBUSTaskHandle;
uint32_t MODBUSTaskBuffer[ 1024 ];
osStaticThreadDef_t MODBUSTaskControlBlock;
const osThreadAttr_t MODBUSTask_attributes = {
  .name = "MODBUSTask",
  .cb_mem = &MODBUSTaskControlBlock,
  .cb_size = sizeof(MODBUSTaskControlBlock),
  .stack_mem = &MODBUSTaskBuffer[0],
  .stack_size = sizeof(MODBUSTaskBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for PWMSem */
osSemaphoreId_t PWMSemHandle;
osStaticSemaphoreDef_t PWMSemControlBlock;
const osSemaphoreAttr_t PWMSem_attributes = {
  .name = "PWMSem",
  .cb_mem = &PWMSemControlBlock,
  .cb_size = sizeof(PWMSemControlBlock),
};
/* Definitions for BSemUART1_RX_ISR */
osSemaphoreId_t BSemUART1_RX_ISRHandle;
osStaticSemaphoreDef_t BSemUART1_RX_ISRControlBlock;
const osSemaphoreAttr_t BSemUART1_RX_ISR_attributes = {
  .name = "BSemUART1_RX_ISR",
  .cb_mem = &BSemUART1_RX_ISRControlBlock,
  .cb_size = sizeof(BSemUART1_RX_ISRControlBlock),
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
extern void CLI(void *argument);
extern void PWM(void *argument);
extern void MODBUS(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of PWMSem */
  PWMSemHandle = osSemaphoreNew(1, 1, &PWMSem_attributes);

  /* creation of BSemUART1_RX_ISR */
  BSemUART1_RX_ISRHandle = osSemaphoreNew(1, 1, &BSemUART1_RX_ISR_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Tcommrx */
  TcommrxHandle = osThreadNew(CLI, NULL, &Tcommrx_attributes);

  /* creation of TPWM */
  TPWMHandle = osThreadNew(PWM, NULL, &TPWM_attributes);

  /* creation of MODBUSTask */
  MODBUSTaskHandle = osThreadNew(MODBUS, NULL, &MODBUSTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

