/*
 * shift.c
 *
 *  Created on: 7 Sep 2020
 *      Author: avinoam.danieli
 */

#include "main.h"
#include "cmsis_os.h"
#include "defs.h"
#include <stdio.h>
#include <string.h>
#include <shift.h>

/********************************************************************
*	shftreg
*
********************************************************************/
int32_t shftreg(int32_t shft, uint16_t num, SHFTREG *plo, SHFTREG *phi)
{
	uint64_t mask;

	SHFTREG lo;
	SHFTREG hi;
	
	if ( (shft > 128) || (num == 0) )
		return TRUE;
   lo = *plo;
   hi = *phi;
	if ( (shft == 0) );
	else
	{
		if (shft > 63)
		{
			lo.ul64 = hi.ul64 >> (shft - 64);
			hi.ul64 = 0;
		}
		else
		{
			lo.ul64 = (lo.ul64 >> shft) | ( hi.ul64 << (64 - shft) );
			hi.ul64 = hi.ul64 >> shft;
		}
	}
	if (num < 64)
	{
		hi.ul64 = 0;
		mask = ~(0xffffffff << num); // ad should be 0xffffffffffffffff
		lo.ul64 &= mask;
	}
	else
	{
		if (num <= 128)
		{
			num -= 64;
			mask = ~(0xffffffff << num);
			hi.ul64 &= mask;
		}
	}
  	//printf("%llx    %llx", lo.ul64, hi.ul64);
   *plo = lo;
   *phi = hi;

	return TRUE;
}

void getcoils(SHFTREG *plo, SHFTREG *phi, int32_t heartbit)
{
   SHFTREG lo;
   SHFTREG hi;

//   lo = *plo;
//   hi = *phi;
   lo.ul64 = 0;
   hi.ul64 = 0;

	hi.ul64 |= ( HAL_GPIO_ReadPin(STO_GPIO_Port, STO_Pin) );    												// 64
	hi.ul64 |= ( HAL_GPIO_ReadPin(SAFETY_ALERT_GPIO_Port, SAFETY_ALERT_Pin) << 1 );    					// 65
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(PBtoMCU_GPIO_Port, PBtoMCU_Pin)) << 33 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(P3V3_PG_GPIO_Port, P3V3_PG_Pin)) << 34 );    				//

	lo.ul64 |= ((uint64_t)( 1 ) << 35 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(P24V_PG_GPIO_Port, P24V_PG_Pin)) << 36 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(DRV_EN_GPIO_Port, DRV_EN_Pin)) << 38 );    				//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(PULLER_EN_GPIO_Port, PULLER_EN_Pin)) << 39 );    		//
	lo.ul64 |= ((uint64_t)( HAL_GPIO_ReadPin(STP_EN_GPIO_Port, STP_EN_Pin)) << 40 );    				//

	lo.ul64 |= ( (uint64_t)( HAL_GPIO_ReadPin(KEY_MCU_GPIO_Port, KEY_MCU_Pin) ) << 53 );    			//
	lo.ul64 |= ( (uint64_t)( testpwmkey() ) << 54 );    				//
	hi.ul64 |= (1 << 8) | (1 << 10) | (1 << 13) | (1 << 15);
	hi.ul64 |= heartbit << 12;

   *plo = lo;
   *phi = hi;
}
