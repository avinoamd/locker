/*
 * ADC.h
 *
 *  Created on: Jul 6, 2020
 *      Author: avinoam.danieli
 */

#ifndef ADC_H_
#define ADC_H_
int32_t CPUTmpr(void);
int32_t CPUTmpr_CLI(CLI_command_T* command);
int32_t IntVref(void);
int32_t IntVref_CLI(CLI_command_T* command);
uint16_t VHall1(void);
uint16_t VHall2(void);
int32_t VHall_CLI(CLI_command_T* command);
int32_t curr_CLI(CLI_command_T* command);
int32_t curr_limit_CLI(CLI_command_T* command);

int32_t getvbatt(float *VSENSE);

int32_t  ADC_DMA_Init(void);

#define ADC_DMA_Buffer_Len 5
extern __IO uint16_t ADC_DMA_Buffer[ADC_DMA_Buffer_Len];

#define HALL1					0		// 
#define CURR					1		// 
#define HALL2					2		// 
#define INTR_TMPR				3		// 
#define INTR_VREF				4		// 

extern int32_t ADC_ConvCplt_Flg; // can be used by one task at a time
extern int32_t h2av;
extern int32_t h1av;
extern int32_t currav;


/*
	IN0	ADC_VBATT
	
*/




#endif /* ADC_H_ */
