/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BOOT0_Pin GPIO_PIN_2
#define BOOT0_GPIO_Port GPIOC
#define LCKR_EN3V3_Pin GPIO_PIN_3
#define LCKR_EN3V3_GPIO_Port GPIOC
#define HS1_Pin GPIO_PIN_0
#define HS1_GPIO_Port GPIOA
#define HS2_Pin GPIO_PIN_1
#define HS2_GPIO_Port GPIOA
#define DIR_Pin GPIO_PIN_2
#define DIR_GPIO_Port GPIOA
#define CURR_Pin GPIO_PIN_3
#define CURR_GPIO_Port GPIOA
#define TESTER_Pin GPIO_PIN_4
#define TESTER_GPIO_Port GPIOC
#define LED_U_RED_Pin GPIO_PIN_5
#define LED_U_RED_GPIO_Port GPIOC
#define nBrake_Pin GPIO_PIN_0
#define nBrake_GPIO_Port GPIOB
#define LED_GREEN_Pin GPIO_PIN_1
#define LED_GREEN_GPIO_Port GPIOB
#define LED_RED_Pin GPIO_PIN_2
#define LED_RED_GPIO_Port GPIOB
#define VSW_Pin GPIO_PIN_14
#define VSW_GPIO_Port GPIOB
#define DRV_EN_Pin GPIO_PIN_8
#define DRV_EN_GPIO_Port GPIOA
#define LOCKER_ID_Pin GPIO_PIN_10
#define LOCKER_ID_GPIO_Port GPIOC
#define HA_Pin GPIO_PIN_6
#define HA_GPIO_Port GPIOB
#define HB_Pin GPIO_PIN_7
#define HB_GPIO_Port GPIOB
#define HC_Pin GPIO_PIN_8
#define HC_GPIO_Port GPIOB
#define nFAULT_Pin GPIO_PIN_9
#define nFAULT_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
