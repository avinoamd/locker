/**
  ******************************************************************************
  * File Name          : defs.h
  * Author             : Ariel
  * Date               : 28/05/2014
  * Description        : generic defination (check for dubbles)
  ******************************************************************************
 */


#ifndef _DEFS_H_
#define _DEFS_H_
#ifndef TRUE
#define TRUE  			0x01
#endif

#ifndef FALSE
#define FALSE 			0x00
#endif

//#define ON         		TRUE
#define OK         		TRUE
//#define OFF				FALSE

#ifndef NULL
#define NULL    		((void*)0)
#endif

#ifndef FOREVER
#define FOREVER  		1
#endif

#ifndef HANDLE
#define HANDLE			(void*)
#endif

#endif /* #ifndef _DEFS_H_ */

/*******************************************************************************
 * End of File
 ********************************************************************************/
