/*
 * SOM_MODBUS.h
 *
 *  Created on: Aug 17, 2020
 *      Author: avinoam.danieli
 */

#ifndef SOM_MODBUS_H_
#define SOM_MODBUS_H_

#define MODBUSINLEN 96
#define MODBUSOUTLEN 128
//#define PLCOUTLEN 128
//extern uint8_t SOMinbuff[SOMINLEN];							// for SOM
//extern uint8_t SOMoutbuff[SOMOUTLEN];						// for SOM
extern int32_t SOMrxflg;
extern uint32_t SOMcnt;
extern uint32_t SOMcntOut, SOMcntTime, SOMcntOutTime;

void uSecDelay(uint16_t us);
void SOM_MODBUS(void);

#endif /* SOM_MODBUS_H_ */
