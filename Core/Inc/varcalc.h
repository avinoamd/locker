/*
 * varcalc.h
 *
 *  Created on: 11 Jan 2022
 *      Author: avinoam.danieli
 */

#ifndef APPLICATION_USER_INC_VARCALC_H_
#define APPLICATION_USER_INC_VARCALC_H_

int32_t find_max_CLI(CLI_command_T* command);
int32_t find_max(void);
int32_t find_min_CLI(CLI_command_T* command);
int32_t find_min(void);
void find_max_isr(void);
void find_min_isr(void);
int32_t varcalc_CLI(CLI_command_T* command);
uint16_t getstatusword(void);

#endif /* APPLICATION_USER_INC_VARCALC_H_ */
