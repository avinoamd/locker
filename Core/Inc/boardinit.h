/*
 * BoardInit.h
 *
 *  Created on: Aug 6, 2020
 *      Author: avinoam.danieli
 */

#ifndef BOARDINIT_H_     
#define BOARDINIT_H_

void BoardInit(void);
int32_t LEDcmd(CLI_command_T* command);
extern int32_t MotorDir;


typedef struct
{
	uint32_t               uwIC2Value1;						// First Input capture
	uint32_t               uwIC2Value2;						// Second Input capture
	uint32_t               uwDiffCapture;

	/* Capture index */
	uint16_t               uhCaptureIndex;

	/* Frequency Value */
	uint32_t               uwFrequency;
	uint32_t          	  avFrequency;
	uint32_t               intcnt;
	int32_t					  dist;
	int32_t					  target;
} Spd_t;

extern Spd_t Spd;
    

#endif /* BOARDINIT_H_ */     
