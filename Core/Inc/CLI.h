/**
  ******************************************************************************
  * File Name          : cli.h
  * Author             : Ariel
  * Date               : 28/07/2014 13:06:09
  * Description        : This file provides code for protocol
  ******************************************************************************
*/

#ifndef ___CLI_H___
#define ___CLI_H___

#include <stdint.h>
//#include "laser.h"
#include "task.h"

#define COMMAND_MAX_LENGTH 64
#define ARGUMENT_MAX_LENGTH 64
#define CLI_COMMANDS        200

typedef enum {

  SETPAR 	= 0,
  GETPAR 	,
  HELP		,
  LOCALHELP

}
ArgType_T;

typedef enum
{
   OKr                    =0,
   COMMAND_NOT_FOUND     ,
   NO_AUTHORIZATION     ,
   OUT_OF_LIMITS        ,
   CALL_ERROR		,
   NO_permission		, //repeated	,
   UNKNOWN		 //7


}CLIRetCode_t;

typedef struct
{
    char        sCommand  [COMMAND_MAX_LENGTH];
    char        sParameter[ARGUMENT_MAX_LENGTH];
    ArgType_T   ArgType;
}CLI_command_T;

typedef enum {

  CLIENT 	= 1,
  TECHNICIAN 	,
  RND,
  DNP,
  RNDONLY
}
PERMIT_LEVEL_T;

typedef struct
{
    /// command name, as it should be typed on CLI prompt.
    char    *pName;

    /// pointer to CLI command handler function.
    int32_t     (*pHandler)(CLI_command_T* command);

    /// max number of command parameters
    int     argCount;

    /// which condition command can execute
    PERMIT_LEVEL_T    permission;


    /// a short command description string to be dislayed by "help" command.
    char    *pDescription;

} CLI_COMMAND_LIST_T;

//CLIRetCode_t CLI_ConvertStringToCommand(char* sStringRecive,CLI_Exe_command_T* pCommand );
CLIRetCode_t CLI_ExecutCommand(CLI_command_T* pCommand ,  CLI_COMMAND_LIST_T const * pCommandList);
void CLIErrorHandler(CLIRetCode_t errorFromList);
void errorHandler(int32_t num, char const * FILE, int LINE, char const * PRETTY_FUNCTION);
int32_t DMA_Ready(UART_HandleTypeDef *huart, int32_t delay);

int32_t	help		(CLI_command_T* command)	;

extern const CLI_COMMAND_LIST_T gCliCommandsList[] /*@ "FLASH"*/ ;
extern TaskHandle_t RxFlg;

#define BYTE_TO_BINARY_PATTERN %c%c%c%c%c%c%c%c
#define BYTE_TO_BINARY(byte)  \
			(byte & 0x80 ? '1' : '0'), \
			(byte & 0x40 ? '1' : '0'), \
			(byte & 0x20 ? '1' : '0'), \
			(byte & 0x10 ? '1' : '0'), \
			(byte & 0x08 ? '1' : '0'), \
			(byte & 0x04 ? '1' : '0'), \
			(byte & 0x02 ? '1' : '0'), \
			(byte & 0x01 ? '1' : '0')

#define WORD_TO_BINARY_PATTERN %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c
#define WORD_TO_BINARY(byte)  \
		  (byte & 0x8000 ? '1' : '0'), \
		  (byte & 0x4000 ? '1' : '0'), \
		  (byte & 0x2000 ? '1' : '0'), \
		  (byte & 0x1000 ? '1' : '0'), \
		  (byte & 0x0800 ? '1' : '0'), \
		  (byte & 0x0400 ? '1' : '0'), \
		  (byte & 0x0200 ? '1' : '0'), \
		  (byte & 0x0100 ? '1' : '0'), \
			(byte & 0x80 ? '1' : '0'), \
			(byte & 0x40 ? '1' : '0'), \
			(byte & 0x20 ? '1' : '0'), \
			(byte & 0x10 ? '1' : '0'), \
			(byte & 0x08 ? '1' : '0'), \
			(byte & 0x04 ? '1' : '0'), \
			(byte & 0x02 ? '1' : '0'), \
			(byte & 0x01 ? '1' : '0')

#define EMBLEN 100
extern char errorMessageBuff[EMBLEN];
#define RXLEN 256
extern uint8_t RxBuff[RXLEN];
#define TXLEN 256
extern uint8_t TxBuff[RXLEN];

void errorMessage(char *str);

#endif
