/*
 * EEPROM.h
 *
 *  Created on: 27 Jan 2022
 *      Author: avinoam.danieli
 */

#ifndef APPLICATION_USER_INC_EEPROM_H_
#define APPLICATION_USER_INC_EEPROM_H_

#define RightLocker 0
#define LeftLocker 1
int32_t getside(void);
int32_t side(CLI_command_T* command);
int32_t sideEn(CLI_command_T* command);

extern int32_t *pSide;
extern int32_t SideEn;

#endif /* APPLICATION_USER_INC_EEPROM_H_ */
