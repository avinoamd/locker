/*
 * CLIops.h
 *
 *  Created on: 10 Dec 2021
 *      Author: avinoam.danieli
 */

#ifndef APPLICATION_USER_INC_CLIOPS_H_
#define APPLICATION_USER_INC_CLIOPS_H_

int32_t enable(CLI_command_T* command);
int32_t move(CLI_command_T* command);
int32_t getposition(CLI_command_T* command);
int32_t speed(CLI_command_T* command);
int32_t status(CLI_command_T* command);
int32_t current(CLI_command_T* command);
int32_t peakcurrent(CLI_command_T* command);
int32_t mhall(CLI_command_T* command);
int32_t getpwm(void);
int32_t setpwm(int32_t var);
int32_t pwm(CLI_command_T* command); 
int32_t setdir(CLI_command_T* command);
int32_t getdir(CLI_command_T* command);
int32_t getspd(CLI_command_T* command);
int32_t nBrake(CLI_command_T* command);
int32_t gof(int32_t command);
int32_t go(CLI_command_T* command);
int32_t dist(CLI_command_T* command);
int32_t target(CLI_command_T* command);

#endif /* APPLICATION_USER_INC_CLIOPS_H_ */
