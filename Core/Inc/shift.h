/*
 * shift.h
 *
 *  Created on: 7 Sep 2020
 *      Author: avinoam.danieli
 */


typedef union {
	uint64_t ul64;
	uint8_t  uc[8];
} SHFTREG;


//extern SHFTREG lo;
//extern SHFTREG hi;
//SHFTREG hio, loo;

int32_t shftreg(int32_t shft, uint16_t num, SHFTREG *plo, SHFTREG *phi);
void getcoils(SHFTREG *plo, SHFTREG *phi, int32_t heartbit);


