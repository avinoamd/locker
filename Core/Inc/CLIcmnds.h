/*
 * CLIcmnds.h
 *
 *  Created on: Jul 1, 2020
 *      Author: avinoam.danieli
 */

#ifndef CLICMNDS_H_
#define CLICMNDS_H_

int32_t GetVersion(CLI_command_T* command);
int32_t Uptime(CLI_command_T* command);
int32_t help(CLI_command_T* command);
int32_t cli_powermode(CLI_command_T* command);
int32_t SerialNumber(CLI_command_T* command);
int32_t message(CLI_command_T* command);  

uint32_t datetimeformain(char *buff);
uint32_t versionformain(char *buff);
int32_t mcudata(CLI_command_T* command);

#define HWVER  " EAS-0014-01"
#define SWVER	"Version 1_04"       
#define FW_MAJOR	1       
#define FW_MINOR	04     

#define REFRESH 29

#endif /* CLICMNDS_H_ */
