/*
 * PLC.h
 *
 *  Created on: Aug 17, 2020
 *      Author: avinoam.danieli
 */

#ifndef PLC_H_
#define PLC_H_

#define PLCINLEN 128
#define PLCOUTLEN 128
//extern uint8_t PLCinbuff[PLCINLEN];						// for PLC
//extern uint8_t PLCoutbuff[PLCOUTLEN];					// for PLC
extern uint32_t PLCcnt;
extern uint32_t PLCcntOut;
extern uint16_t PLCTimeout;
extern uint32_t PLCcntTime;
extern uint32_t PLCcntOutTime;

typedef struct Modbus
{
	uint8_t *inbuff;
	uint8_t *outbuff;
	uint32_t *cnt;
	uint32_t *cnt_time;
   int32_t heartbit;
   UART_HandleTypeDef *huart;
} STModbus;


void uS_Delay(uint32_t us);
#include "CLI.h"
int32_t plc_timeout(CLI_command_T* command);
void PLCcmnds(STModbus *modbus);
int32_t PLC_test_CLI(CLI_command_T* command);

#endif /* PLC_H_ */
